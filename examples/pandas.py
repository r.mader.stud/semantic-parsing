from semantic_pandas import SemanticParser, SklearnParseScorer
from semantic_pandas.evaluators import PandasSemanticEvaluator
from semantic_pandas.grammars import pandasGrammar
from semantic_pandas.utility import loadResource

scaler = None
reducer = loadResource("none_pca_reducer_lg")
scoringModel = loadResource("none_pca_perceptron_model_lg")

pandasGrammar.similarityThreshold = 1

scorer = SklearnParseScorer(
            scoringModel,
            lambda parse: parse.featureVector(pandasGrammar),
            [reducer])

parser = SemanticParser(pandasGrammar, scorer)
evaluator = PandasSemanticEvaluator()

print(
    "Enter your natural language intent to display the parsed code snippet."
    "Type 'exit' to end the program.")

exitRequested = False
while not exitRequested:
    inputString = input("> ")

    if inputString == "exit":
        exitRequested = True
        continue

    parses = parser.parseAll(inputString)[:5]

    for i, parse in enumerate(parses):
        snippet = evaluator.evaluate(parse)

        print(f"Snippet {i + 1}: {snippet}")
