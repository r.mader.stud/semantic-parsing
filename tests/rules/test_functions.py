import pytest

from semantic_pandas.rules import makeRule, LexicalRule, UnaryRule, BinaryRule, NAryRule
from semantic_pandas.semantics import SimpleSemantic


class TestFunctions:
    @pytest.mark.parametrize("lhs, rhs, semantic", [
        ("$A", "B", SimpleSemantic(0)),
        ("$A", ("B",), SimpleSemantic(0))
    ])
    def testMakeRuleLexical(self, lhs, rhs, semantic):
        rule = makeRule(lhs, rhs, semantic)

        assert isinstance(rule, LexicalRule)
        assert rule.lhs == lhs
        assert rule.rhs == rhs if isinstance(lhs, tuple) else (lhs,)
        assert rule.semantic == semantic

    @pytest.mark.parametrize("lhs, rhs, semantic", [
        ("$A", "$B", SimpleSemantic(0)),
        ("$A", ("$B",), SimpleSemantic(0))
    ])
    def testMakeRuleUnary(self, lhs, rhs, semantic):
        rule = makeRule(lhs, rhs, semantic)

        assert isinstance(rule, UnaryRule)
        assert rule.lhs == lhs
        assert rule.rhs == rhs if isinstance(rhs, tuple) else (rhs,)
        assert rule.semantic == semantic

    @pytest.mark.parametrize("lhs, rhs, semantic", [
        ("$A", ("$B", "$C"), SimpleSemantic(0))
    ])
    def testMakeRuleBinaryRule(self, lhs, rhs, semantic):
        rule = makeRule(lhs, rhs, semantic)

        assert isinstance(rule, BinaryRule)
        assert rule.lhs == lhs
        assert rule.rhs == rhs
        assert rule.semantic == semantic

    @pytest.mark.parametrize("lhs, rhs, semantic", [
        ("$A", ("$B", "$C", "$D"), SimpleSemantic(0)),
        ("$A", ("$B", "$C", "$D", "$E"), SimpleSemantic(0))
    ])
    def testMakeRuleNAryRule(self, lhs, rhs, semantic):
        rule = makeRule(lhs, rhs, semantic)

        assert isinstance(rule, NAryRule)
        assert rule.lhs == lhs
        assert rule.rhs == rhs
        assert rule.semantic == semantic

    @pytest.mark.parametrize("lhs, rhs, semantic", [
        ("$A", ("$B", "C"), SimpleSemantic(0))
    ])
    def testMakeRuleInvalid(self, lhs, rhs, semantic):
        with pytest.raises(ValueError):
            makeRule(lhs, rhs, semantic)
