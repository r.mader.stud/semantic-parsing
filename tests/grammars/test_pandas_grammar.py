import pytest

from semantic_pandas.datasets.manual_pandas_queries import manualPandasQueries
from semantic_pandas.grammars.pandas_grammar import pandasGrammar
from semantic_pandas.evaluators import PandasSemanticEvaluator


class TestPandasGrammar:
    @pytest.mark.parametrize("dataset", [manualPandasQueries])
    def testPandasGrammarCanParseAllDatasetQueries(self, dataset):
        evaluator = PandasSemanticEvaluator()

        for entry in dataset:
            intent = entry.intent
            snippet = entry.snippet

            parses = pandasGrammar.parse(intent)
            for parse in parses:
                evaluatedSnippet = evaluator.evaluate(parse)
                if evaluatedSnippet == snippet:
                    break
            else:
                pytest.fail()
