import pytest
from collections import namedtuple

from semantic_pandas.evaluators import PandasSemanticEvaluator

DummyParse = namedtuple("DummyParse", "semantics")


class TestPandasSemanticEvaluator:
    @pytest.mark.parametrize("semantics, expectedSnippet", [
        # create operation
        (
            DummyParse({
                "domain": "pandas",
                "operation": "create",
                "type": "Dataframe",
                "from": {"variable": "dict"},
                "index": {"token": "index"},
                "assignTo": {"variable": "df"}
            }),
            "df = Dataframe(dict, index=index)"
        ),
        # select operations
        (
            DummyParse({
                "domain": "pandas",
                "operation": "select",
                "selectType": "column",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "columnName": {"variable": "col"},
                "assignTo": {"variable": "result"}
            }),
            "result = df[col]"
        ),
        (
            DummyParse({
                "domain": "pandas",
                "operation": "select",
                "selectType": "row",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "condition": {
                    "lhs": {"variable": "var"},
                    "rhs": {"variable": "3"},
                    "operator": ">"
                },
                "assignTo": {"variable": "result"}
            }),
            "result = df[var > 3]"
        ),
        (
            DummyParse({
                "domain": "pandas",
                "operation": "select",
                "selectType": "row",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "head": {"value": 10},
                "assignTo": {"variable": "result"}
            }),
            "result = df.head(10)"
        ),
        (
            DummyParse({
                "domain": "pandas",
                "operation": "select",
                "selectType": "row",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "tail": {"value": 10},
                "assignTo": {"variable": "result"}
            }),
            "result = df.tail(10)"
        ),
        (
            DummyParse({
                "domain": "pandas",
                "operation": "select",
                "selectType": "row",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "range": {
                    "from": {"value": 0},
                    "to": {"value": 10}
                },
                "assignTo": {"variable": "result"}
            }),
            "result = df[0:10]"
        ),
        (
            DummyParse({
                "domain": "pandas",
                "operation": "select",
                "selectType": "maximum",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "subSelectSource": {"typeVariable": {"variable": "col"}},
                "assignTo": {"variable": "result"}
            }),
            "result = df[col].max()"
        ),
        (
            DummyParse({
                "domain": "pandas",
                "operation": "select",
                "selectType": "minimum",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "subSelectSource": {"typeVariable": {"variable": "col"}},
                "assignTo": {"variable": "result"}
            }),
            "result = df[col].min()"
        ),
        (
            DummyParse({
                "domain": "pandas",
                "operation": "select",
                "selectType": "average",
                "selectSource": {"typeVariable": {"variable": "df"}},
                "subSelectSource": {"typeVariable": {"variable": "col"}},
                "assignTo": {"variable": "result"}
            }),
            "result = df[col].mean()"
        ),
        # convert operations
        (
            DummyParse({
                "domain": "pandas",
                "operation": "convert",
                "convertFormat": {"format": "csv"},
                "typeVariable": {"variable": "df"},
                "assignTo": {"variable": "result"}
            }),
            "result = df.to_csv()"
        ),
        # append operations
        (
            DummyParse({
                "domain": "pandas",
                "operation": "sort",
                "typeVariable": {"variable": "df"},
                "orderBy": {"variable": "col"},
                "assignTo": {"variable": "result"}
            }),
            "result = df.sort_values(col)"
        ),
        (DummyParse({"domain": "other"}), None)
    ])
    def testEvaluate(self, semantics, expectedSnippet):
        evaluator = PandasSemanticEvaluator()

        evaluatedSnippet = evaluator.evaluate(semantics)

        assert evaluatedSnippet == expectedSnippet
