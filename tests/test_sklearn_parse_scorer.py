from collections import namedtuple

from semantic_pandas import SklearnParseScorer

DummyModel = namedtuple("DummyModel", ["fit", "fit_transform", "transform", "predict"])


class TestSklearnParseScorer:
    def testCall(self):
        value = 1
        dummyScoringModel = DummyModel(None, None, None, lambda x: [x[0] * 3])
        dummyTransformModel1 = DummyModel(None, None, lambda x: [x[0] + 1], None)
        dummyTransformModel2 = DummyModel(None, None, lambda x: [x[0] * 2], None)

        scorer = SklearnParseScorer(
            dummyScoringModel,
            lambda parse: parse,
            [dummyTransformModel1, dummyTransformModel2]
        )

        transformedValue = dummyScoringModel.predict(
            dummyTransformModel2.transform(
                dummyTransformModel1.transform([scorer.toFeatureVector(value)])
            )
        )[0]

        assert scorer(value) == transformedValue

    def testTransformFeatureVectors(self):
        value = [1]
        dummyTransformModel1 = DummyModel(None, None, lambda x: [x[0] + 1], None)
        dummyTransformModel2 = DummyModel(None, None, lambda x: [x[0] * 2], None)
        transformedValue = dummyTransformModel2.transform(dummyTransformModel1.transform(value))

        scorer = SklearnParseScorer(None, None, [dummyTransformModel1, dummyTransformModel2])

        assert scorer.transformFeatureVectors(value) == transformedValue
