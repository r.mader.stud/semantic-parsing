import pytest

from semantic_pandas.semantics import CustomSemantic


class TestCustomSemantic:
    def testCompute(self):
        value = {"value": "key"}

        semantic = CustomSemantic(lambda a: a[0])

        assert semantic.compute([value]) == value

    def testComputeInvalid(self):
        semantic = CustomSemantic(None)

        with pytest.raises(TypeError):
            semantic.compute([])
