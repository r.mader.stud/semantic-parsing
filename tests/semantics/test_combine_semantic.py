import pytest

from semantic_pandas.semantics import CombineSemantic


class TestCombineSemantic:
    @pytest.mark.parametrize(
        "combinePositions, additionals, semanticValues, computedValues", [
            (
                [0],
                None,
                [{"value1": "key1"}],
                {"value1": "key1"}
            ),
            (
                [0, 1],
                None,
                [{"value1": "key1"}, {"value2": "key2"}],
                {"value1": "key1", "value2": "key2"}
            ),
            (
                [0],
                {"value2": "key2"},
                [{"value1": "key1"}],
                {"value1": "key1", "value2": "key2"}
            )
        ]
    )
    def testCompute(
            self, combinePositions, additionals, semanticValues, computedValues
            ):
        semantic = CombineSemantic(combinePositions, additionals)

        assert semantic.compute(semanticValues) == computedValues

    @pytest.mark.parametrize(
        "combinePositions, additionals, semanticValues", [
            ([1], None, [{"value1": "key1"}]),
            ([0], None, [])
        ]
    )
    def testComputeInvalid(self, combinePositions, additionals, semanticValues):
        semantic = CombineSemantic(combinePositions, additionals)

        with pytest.raises(IndexError):
            semantic.compute(semanticValues)
