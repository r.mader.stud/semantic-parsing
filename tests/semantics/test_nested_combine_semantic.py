import pytest

from semantic_pandas.semantics import NestedCombineSemantic


class TestNestedCombineSemantic:
    @pytest.mark.parametrize(
        "labelMapping, combinePositions, additionals, semanticValues, computedValues", [
            (
                {"label": 0},
                [1],
                None,
                [{"value1": "key1"}, {"value2": "key2"}],
                {"label": {"value1": "key1"}, "value2": "key2"}
            ),
            (
                {"label": 0},
                [],
                {"value2": "key2"},
                [{"value1": "key1"}, {"value2": "key2"}],
                {"label": {"value1": "key1"}, "value2": "key2"}
            )
        ]
    )
    def testCompute(
            self, labelMapping, combinePositions, additionals, semanticValues, computedValues
            ):
        semantic = NestedCombineSemantic(labelMapping, combinePositions, additionals)

        assert semantic.compute(semanticValues) == computedValues

    @pytest.mark.parametrize(
        "labelMapping, combinePositions, additionals, semanticValues", [
            (
                {"label": 0},
                [1],
                None,
                [{"value1": "key1"}]
            ),
            (
                {"label": 1},
                [0],
                None,
                [{"value1": "key1"}]
            )
        ]
    )
    def testComputeInvalid(self, labelMapping, combinePositions, additionals, semanticValues):
        semantic = NestedCombineSemantic(labelMapping, combinePositions, additionals)

        with pytest.raises(IndexError):
            semantic.compute(semanticValues)
