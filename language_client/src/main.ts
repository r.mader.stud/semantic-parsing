'use strict';

import * as path from "path";
import { Socket } from "net";
import { workspace, ExtensionContext } from "vscode";
import { LanguageClient, LanguageClientOptions, ServerOptions} from "vscode-languageclient";

/*
 * The following code has been heavily inspired from the examples of the "pygls" python extension:
 * See https://github.com/openlawlibrary/pygls/blob/master/examples/json-extension/client/src/extension.ts
*/

// configuration variables
// NOTE: make sure to also set this value when starting the language server
const TCP_PORT: number = 8080;

// NOTE: variable is global in order to be referenced in different entrypoints used by vscode
var client: LanguageClient;


function isDebugMode(): boolean {
    return process.env["VSCODE_DEBUG_MODE"] == "true";
}

function getClientOptions(): LanguageClientOptions {
	return {
		documentSelector: ["python"],
        synchronize: {
            configurationSection: "semantic_pandas"
        }
    }
}

function createLanguageClientTcp(port: number): LanguageClient {
    const serverOptions: ServerOptions = () => {
        return new Promise((resolve, reject) => {
            const clientSocket = new Socket();
            clientSocket.connect(port, "127.0.0.1", () => {
                resolve({
                    reader: clientSocket,
                    writer: clientSocket
                });
            });
        });
    }

    return new LanguageClient(`tcp lang server port (port ${port})`, serverOptions, getClientOptions());
}

function createLanguageClientIo(): LanguageClient {
    const cwd = path.join(__dirname, "..", "..");
    // NOTE: must use "any"-type cast here since "getConfiguration" can use string as second parameter, but is not typed correctly
    const pythonPath = workspace.getConfiguration("python", <any> "null").get<string>("pythonPath");

    const serverOptions: ServerOptions = {
        args: ["-m", "semantic_pandas.language_server"],
        command: pythonPath,
        options: { cwd },
      };

    return new LanguageClient(pythonPath, serverOptions, getClientOptions());
}

export function activate(context: ExtensionContext) {
    console.log("Extension activated");

    if (isDebugMode()) {
        console.log("Starting in debug mode");
        // NOTE: server is not started automatically in debug and must be started separately

        // use TCP protocol in debug mode
        client = createLanguageClientTcp(TCP_PORT);
    }
    else {
        console.log("Starting in production mode");

        // use IO protocol in production
        client = createLanguageClientIo();
    }

    context.subscriptions.push(client.start());
}

export function deactivate(): Thenable<void> {
    console.log("Extension deactivated");
    return client ? client.stop() : Promise.resolve();
}
