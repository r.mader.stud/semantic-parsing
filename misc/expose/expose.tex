\documentclass{article}

\usepackage[a4paper, total={6in, 10in}]{geometry}
\usepackage{url}

\title{\textbf{Python code recommendation from natural language using semantic parsing} \\  
	\Large{Exposé for a bachelor thesis}
}
\author{By Robin Mader \\ Supervisor: Prof. Dr. Artur Andrzejak}
\date{March 21, 2020}

\begin{document}
	\maketitle

	\section*{Domain}
	Semantic parsing (SP) is the process of converting the complete meaning of a natural language (NL) expression into a machine-understandable representation. It can be used to implement an interface that allows users to communicate with an application via natural language. Common examples are question answering applications which transform natural language into database queries.
	
	
	\section*{Problem}
	Programming with libraries like "pandas" in a data science environment often involves the use of common code snippets for manipulating pandas data frames. This leads to repetitive code and requires the user to either be familiar with the pandas library or look up the required snippets. Rewriting or even copying these snippets every time is also error-prone and may result in inconsistent code. 
	
	
	\section*{Goal}
	The goal of this thesis is to analyse the possibilities of using SP to create a code recommender system, which identifies user intent from NL and automatically recommends a corresponding code snippet.
Furthermore, the implementation of a natural language interface for such a system and the subsequent integration into an IDE should be investigated, preferably by creating an appropriate implementation.
	
	
	\section*{Approaches and tools}
	\begin{itemize}
		\item \textbf{Programming Language}: The Python programming language offers a multitude of tools and examples for NLP and allows for fast and simple implementation. Since performance is not the focus of this thesis, Python is a suitable environment.
		\item \textbf{Semantic Parser}: A simple and narrow semantic parser can be implemented in Python without any extra tools or frameworks as demonstrated by the SippyCup project\footnote{\url{https://github.com/wcmac/sippycup}}, which uses a grammar-based approach to parse NL expressions.
		\item \textbf{Natural Language Tools}: The NLTK and spaCy packages provide many useful and easy to use functionalities for the processing of NL expressions (e.g. tokenization, lemmatization).
		\item \textbf{Machine Learning Tools}: The spaCy and scikit-learn packages allow for fast and easy training and use of common machine learning models.
		\item \textbf{Simplification}: Since SP becomes significantly easier by reducing the domain of the NL expressions this thesis will focus on providing SP and code recommendation for the commonly used Python package pandas.
		\item \textbf{IDE Integration}: Since Visual Studio Code is a commonly used IDE for Python development and plugins can be created using the Language Server Protocol which is portable to other IDEs, this thesis focuses on these systems to achieve IDE integration.
	\end{itemize}
	
	
	\section*{Work packages}
	In order of complexity
	
		\subsection*{1. Gathering of code snippets}
		In order to identify common intents and create corresponding templates, commonly used pandas operations/snippets must be identified. This can be done in a number of ways, including but not limited to asking experienced pandas users, referencing the pandas documentation or referencing similar projects (e.g. bamboolib\footnote{\url{https://github.com/tkrabel/bamboolib}}).
	
		\subsection*{2. Visual Studio Code integration}
		To allow for the functionality of the developed Python package to be available in Visual Studio Code the Python package can be extended to support the Language Server Protocol, which allows it to be integrated as an extension.
			
		\subsection*{3. Machine learning}
		Since a semantic parsing algorithm usually yields multiple different meaning representations for a single sentence. A machine learning model can be used to identify the most probable meaning representation. Below are the tasks necessary to implement this approach (in order of complexity).
		
			\subsubsection*{Decode output}
			The output of the machine learning should ideally be a single value which denotes the likelihood of the given meaning representation being the actual meaning of the corresponding NL expression. I.e. among multiple different meaning representations, the one with the highest score is most likely the intended meaning of the corresponding NL expression.
			
			\subsubsection*{Using the model}
			Using a machine learning model requires the model to first be trained, this also implies the collection of training data. The Python package and machine learning framework "scikit-learn" can be used to provide the implementation of a machine learning model and corresponding training utilities. A multitude of machine learning models can be used to achieve the task described in this section, this work will focus on using a simple neural network for this task.
			
			\subsubsection*{Encode input}
			In order for a machine learning model to consume the parsed output of the grammar (i.e. a meaning representation) it must be transformed into a suitable format. Most machine learning models accept a vector of a given length as input, therefore our meaning representations must be encoded to such a vector (this task is better known as "feature-engineering"). For CCG grammars a straightforward way of encoding a meaning representation as a vector is to use the rules of the grammar as features and the count of their occurrences in a given meaning representation as values. The "order" (i.e. how close a rule is to the root of the parse tree) should be reflected in the output vector since different orders of rules denote different meaning representations.
			
		\subsection*{4. Create grammar}
		Creating a grammar is the most crucial and complex task of a semantic parsing algorithm. It entails the definition of several rules based on the expected input NL expressions. This work will focus on creating a combinatory categorial grammar\footnote{\url{https://en.wikipedia.org/wiki/Combinatory_categorial_grammar}} (CCG) which parses an NL expression into one or more different meaning representations (as Abstract Meaning Representation\footnote{\url{https://en.wikipedia.org/wiki/Abstract_Meaning_Representation}} based graphs). The NLTK and spaCy Python package offer functionalities (e.g. tokenization, lemmatization, part-of-speech tagging) which can be used to improve the quality of the output and reduce the necessary effort of this work package.


	\section*{Advanced Work packages}
	These work packages describe future work which can be completed either separate from this thesis or as a part of this thesis, should the workload of this thesis turn out to be considerably less than expected.
	
		\subsection*{Compare approaches}
		Semantic parsing can be implemented in multiple different ways. The approaches outlined above are the most promising implementations but not necessarily the best. Listed below are some alternative approaches and implementations which might increase the quality of the semantic parser (in order of complexity).
		
			\subsubsection*{Alternate machine learning models}
			The used machine learning model can often greatly affect the result of a certain task. Therefore it could proof insightful to use and compare the results of using different models. The Python package scikit-learn provides a multitude of different machine learning model implementations which can often be used as almost drop-in replacements.
			
			\subsubsection*{spaCy intent parser}
			The Python package spaCy offers built-in capabilities for training a custom dependency-tree-based parser which can be used to identify semantics in the form of relations between text tokens. This is not the same as a comprehensive meaning representation but is often much simpler to use and can prove satisfactory for applications where a comprehensive meaning representation is not required.
			
			\subsubsection*{Dependency grammar}
			Instead of a CCG a dependency-based grammar can be used instead. The Python packages NLTK and spaCy offer builtin parsing capabilities for dependency trees. This change will also require a new algorithm for encoding dependency trees as vectors, adjusted training data and a correspondingly trained machine learning model.			
			
\end{document}
