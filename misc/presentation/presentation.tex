% \documentclass[handout, aspectratio=169]{beamer} % use to disable pauses
\documentclass[aspectratio=169]{beamer}
\usepackage{pifont}
\usepackage{relsize}
\usepackage{tikz}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{makecell}
\usepackage[T1]{fontenc}
\usepackage[many]{tcolorbox}
\usepackage{lmodern}
\usepackage{soul}
\usepackage{multicol} % for multicol environment
\usepackage{amsmath} % for bmatrix

\tcbuselibrary{listings}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{calc}
\graphicspath{ {../thesis/img/} }

% COLORS
\definecolor{primarythemecolor}{rgb}{0, 0, 0.5}
\definecolor{mygreen}{rgb}{0, 0.7, 0}

% THEME AND THEME CUSTOMIZATION
\usetheme{CambridgeUS}
\useinnertheme{rectangles}
\useoutertheme{infolines}
\usecolortheme[named=primarythemecolor]{structure}
\setbeamercolor{frametitle}{fg=primarythemecolor}
\setbeamercolor{section in head/foot}{bg=primarythemecolor}
\setbeamercolor{subsection in head/foot}{fg=primarythemecolor}
\setbeamercolor{author in head/foot}{bg=primarythemecolor}
\setbeamercolor{date in head/foot}{fg=primarythemecolor}
\setbeamercolor{title in head/foot}{fg=primarythemecolor}
\setbeamercolor{titlelike}{parent=structure, use=palette secondary, fg=primarythemecolor, bg=palette secondary.bg}
\setbeamercolor{block title}{parent=structure, use=palette primary, bg=palette primary.bg}
\setbeamercolor{block body}{parent=structure, use=palette tertiary, fg=black, bg=palette tertiary.fg}

\beamertemplatenavigationsymbolsempty
\AtBeginSection[]{ %Section Slide at begin of new section
	% \begin{frame}
	% 	\vfill
	% 	\centering
	% 	\begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
	% 	\usebeamerfont{title}\insertsectionhead\par%
	% 	\end{beamercolorbox}
	% 	\vfill
	% \end{frame}

	\ifnum \value{framenumber}>3 % exclude first section
		\begin{frame}
			\frametitle{Outline}
			\tableofcontents[currentsection, hideallsubsections]
		\end{frame}
	\else
	\fi
}

% CUSTOM COMMANDS
\newcommand{\cmark}{\textcolor{green}{\ding{51}}}
\newcommand{\xmark}{\textcolor{red}{\ding{55}}}
\newcommand{\Hilight}{\makebox[0pt][l]{\color{cyan}\rule[-4pt]{0.65\linewidth}{14pt}}}
\newcommand<>{\coloronly}[4]{\only<#1>{#4}\only<#2>{\textcolor{#3}{#4}}}
\let\oldurl\url
\renewcommand{\url}[1]{\small{\oldurl{#1}}}
\makeatletter
\let\HL\hl
\renewcommand\hl{%
  \let\set@color\beamerorig@set@color
  \let\reset@color\beamerorig@reset@color
  \HL}
\makeatother

% LISTINGS CONFIGURATION
\newtcblisting{mylisting}{
	colback=lightgray!20,
	boxrule=0pt,
	arc=0pt,
	outer arc=0pt,
	top=-.7em,
	bottom=-.7em,
	colframe=white,
	listing only,
	left=15.5pt,
 	enhanced,
	listing options={
		basicstyle=\scriptsize\ttfamily,
		numbers=left,
		numberstyle=\ttfamily,
    	keywordstyle=\color{blue},
    	commentstyle=\color{mygreen},
    	language=Python,
    	showstringspaces=false,
    	tabsize=2,
    	numbers=left,
    	escapeinside=||
	},
	overlay={
		\fill[lightgray!50] 
			([xshift=-3pt]frame.south west)
			rectangle 
			([xshift=11.5pt]frame.north west);
  	}
}

% METADATA
\title[Code recommendation from NL using SP]{
	Python code recommendation from natural language using semantic parsing
}
\subtitle{Based on combinatory categorial grammars}
\author{Robin Mader}
\institute[University of Heidelberg]{
	University of Heidelberg \\
	Parallel and Distributed Systems Group
}
\date[2020-12-09]{December 9th, 2020} 
	
\begin{document}

	\begin{frame}
		\titlepage
	\end{frame}

%----------------------------------------

	\section{Introduction}
		\subsection{Motivation}
		\begin{frame}
			\frametitle{Code generation from natural language}
			\vfill

			\begin{figure}[ht]
				\begin{center}
					\resizebox{\textwidth}{!}{
						\begin{tikzpicture}
							\node[draw, text width=6cm, align=center, minimum height=1.5cm,
								minimum width=6cm, rounded corners=4mm]
								(naturalLanguage)
								at (0, 0)
								{
									\textbf{Natural Language}\\
									\small\textit{"select column 'id' from dataframe df"}
								};
							\node[draw, text width=2cm, align=center, minimum height=1.5cm,
								minimum width=2cm]
								(parser)
								at (5.5, 0)
								{\textbf{Parser}};
							\node[draw, text width=6cm, align=center, minimum height=1.5cm,
								minimum width=6cm, rounded corners=4mm]
								(semanticRepresentation)
								at (11, 0)
								{
									\textbf{Source code}\\
									\texttt{df['id']}
								};

							\draw[->, line width=.7mm]
								(naturalLanguage.east) -- (parser.west);
							\draw[->,line width=.7mm]
								(parser.east) -- (semanticRepresentation.west);
						\end{tikzpicture}
					}
				\end{center}
			\end{figure}

			\vfill

			\pause

			\textbf{Use cases}
			\begin{itemize}
				\item Accessibility
				\item Education
				\item Convenience
			\end{itemize}

			\vfill
		\end{frame}
		
%----------------------------------------

		\subsection{Background}
		\begin{frame}
			\frametitle{Semantic Parsing}

			\begin{block}{Semantic Parsing}
				Extracting meaning from natural language utterances into a machine understandable
				representation.
			\end{block}

			\pause

			\begin{figure}[ht]
				\begin{center}
					\resizebox{\textwidth}{!}{
						\begin{tikzpicture}
							\node[draw, text width=6cm, align=center, minimum height=1.5cm,
								minimum width=6cm, rounded corners=4mm]
								(naturalLanguage)
								at (0, 0)
								{
									\textbf{Natural Language}\\
									\textit{"How big is New York?"}
								};
							\node[draw, text width=2cm, align=center, minimum height=1.5cm,
								minimum width=2cm]
								(semanticParser)
								at (5.5, 0)
								{\textbf{Semantic Parser}};
							\node[draw, text width=6cm, align=center, minimum height=1.5cm,
								minimum width=6cm, rounded corners=4mm]
								(semanticRepresentation)
								at (11, 0)
								{
									\textbf{Semantic Representation}\\
									\{intent: \textit{"question/area"},\\
									entity: \textit{"city/New\_York\_City"}\}
								};

							\draw[->, line width=.7mm]
								(naturalLanguage.east) -- (semanticParser.west);
							\draw[->,line width=.7mm]
								(semanticParser.east) -- (semanticRepresentation.west);
						\end{tikzpicture}
					}
				\end{center}
			\end{figure}

			\pause

			\textbf{Problem:} Natural language can be ambiguous
			\begin{itemize}
				\item "big": land area or population size?
				\item "New York": state or city?
			\end{itemize}
		\end{frame}

%----------------------------------------

		\begin{frame}
			\frametitle{State of the art: TRANX}
			\begin{block}{TRANX}
				A \textbf{TRAN}sition-based abstract synta\textbf{X} parser for general purpose
				semantic parsing.\\
				Based on ASDL grammars and LSTM encoder-decoder networks.
			\end{block}

			\pause

			\vskip-1em

			\begin{columns}
				\begin{column}{0.7\textwidth}
					\begin{figure}[ht]
						\resizebox{\textwidth}{!}{
							\begin{tikzpicture}
								\node[draw, align=center, minimum height=1.5cm,
									minimum width=6cm, rounded corners=4mm]
									(naturalLanguage)
									at (0, 1.25)
									{
										\textbf{Natural language}\\
										\small\textit{"select column ’id’ from dataframe df"}
									};
								\node[draw, fill=blue!20, align=center, minimum height=1.5cm,
									minimum width=6cm, rounded corners=4mm]
									(asdlGrammar)
									at (0, -1.25)
									{\textbf{ASDL grammar}};
								\node[draw, text width=2cm, align=center, minimum height=4cm,
									minimum width=2cm]
									(transitionSystem)
									at (5.5, 0)
									{\textbf{Transition system}};
								\node[draw, align=center, minimum height=1.5cm, rounded corners=4mm,
									text width=3.5cm]
									(abstractSyntaxTreeCollection)
									at (10, 1.25)
									{\textbf{Abstract syntax tree collection}};
								\node[draw, fill=blue!20, align=center, minimum height=1.5cm,
									rounded corners=4mm, text width=3.5cm]
									(encoderDecoderNetwork)
									at (10, -1.25)
									{\textbf{Encoder-decoder network}};
								\node[draw, text width=2cm, align=center, minimum height=4cm,
									minimum width=2cm]
									(astScorer)
									at (14.25, 0)
									{\textbf{AST scorer}};
								\node[draw, align=center, minimum height=1.5cm, rounded corners=4mm,
									text width=3.5cm]
									(abstractSyntaxTree)
									at (13.5, -4)
									{\textbf{Abstract syntax tree}};
								\node[draw, fill=blue!20, text width=3cm, align=center,
									minimum height=1.5cm, minimum width=3cm]
									(astToMr)
									at (6.5, -4)
									{\textbf{AST\_TO\_MR function}};
								\node[draw, align=center, minimum height=1.5cm, rounded corners=4mm]
									(meaningRepresentation)
									at (-0.8, -4)
									{
										\textbf{Meaning representation}\\
										\texttt{df[’id’]}
									};

								\draw[->, line width=.7mm]
									(naturalLanguage.east) --
									(naturalLanguage-|transitionSystem.west);
								\draw[->, line width=.7mm]
									(asdlGrammar.east) -- (asdlGrammar-|transitionSystem.west);
								\draw[->,line width=.7mm]
									(abstractSyntaxTreeCollection-|transitionSystem.east) --
									(abstractSyntaxTreeCollection.west);
								\draw[->,line width=.7mm]
									(abstractSyntaxTreeCollection.east) --
									(abstractSyntaxTreeCollection-|astScorer.west);
								\draw[->,line width=.7mm]
									(encoderDecoderNetwork.east) --
									(encoderDecoderNetwork-|astScorer.west);
								\draw[->,line width=.7mm]
									(astScorer.south) -- (astScorer|-abstractSyntaxTree.north);
								\draw[->,line width=.7mm]
									(abstractSyntaxTree.west) -- (astToMr.east);
								\draw[->,line width=.7mm]
									(astToMr.west) -- (meaningRepresentation.east);
							\end{tikzpicture}
						}
					\end{figure}
				\end{column}
				\begin{column}{0.3\textwidth}
					\textcolor{blue!20}{$\blacksquare$}: Domain specific, external knowledge
				\end{column}
			\end{columns}
			\vfill
		\end{frame}

%----------------------------------------

	\section{Implementation}
		\subsection{Overview}
		\begin{frame}
			\frametitle{Implementation}
			\begin{figure}[ht]
				\resizebox{\textwidth}{!}{
					\begin{tikzpicture}
						\node[draw, align=center, minimum height=1.5cm,
							minimum width=6cm, rounded corners=4mm]
							(naturalLanguage)
							at (0, 1.25)
							{
								\textbf{Natural language}\\
								\small\textit{"select column ’id’ from dataframe df"}
							};
						\node[draw, fill=blue!20, align=center, minimum height=1.5cm,
							minimum width=6cm, rounded corners=4mm]
							(ccgGrammar)
							at (0, -1.25)
							{\textbf{CCG grammar}};
						\node[draw, text width=2cm, align=center, minimum height=4cm,
							minimum width=2cm]
							(parsingAlgorithm)
							at (5.5, 0)
							{\textbf{Parsing algorithm}};
						\node[draw, align=center, minimum height=1.5cm, rounded corners=4mm,
							text width=3.5cm]
							(parseTreeCollection)
							at (10, 1.25)
							{\textbf{Parse tree collection}};
						\node[draw, fill=blue!20, align=center, minimum height=1.5cm,
							rounded corners=4mm, text width=3.5cm]
							(machineLearningModels)
							at (10, -1.25)
							{\textbf{Machine learning model(s)}};
						\node[draw, text width=2cm, align=center, minimum height=4cm,
							minimum width=2cm]
							(parseScorer)
							at (14.25, 0)
							{\textbf{Parse scorer}};
						\node[draw, align=center, minimum height=1.5cm, rounded corners=4mm,
							text width=2cm]
							(parseTree)
							at (14.25, -4)
							{\textbf{Parse tree}};
						\node[draw, text width=2cm, align=center, minimum height=1.5cm,
							minimum width=2cm]
							(semanticsGenerator)
							at (11.1, -4)
							{\textbf{Semantics generator}};
						\node[draw, align=center, minimum height=1.5cm, rounded corners=4mm,
							text width=3.5cm]
							(semanticRepresentation)
							at (7.25, -4)
							{
								\textbf{Semantic representation}\\
								\footnotesize\texttt{\{'column': 'id', ...\}}
							};
						\node[draw, fill=blue!20, text width=2cm, align=center,
							minimum height=1.5cm, minimum width=2cm]
							(evaluator)
							at (3.4, -4)
							{\textbf{Evaluator}};
						\node[draw, align=center, minimum height=1.5cm, rounded corners=4mm]
							(meaningRepresentation)
							at (-0.8, -4)
							{
								\textbf{Meaning representation}\\
								\texttt{df[’id’]}
							};

						\draw[->, line width=.7mm]
							(naturalLanguage.east) --
							(naturalLanguage-|parsingAlgorithm.west);
						\draw[->, line width=.7mm]
							(ccgGrammar.east) -- (ccgGrammar-|parsingAlgorithm.west);
						\draw[->, line width=.7mm]
							(parseTreeCollection-|parsingAlgorithm.east) --
							(parseTreeCollection.west);
						\draw[->,line width=.7mm]
							(parseTreeCollection.east) --
							(parseTreeCollection-|parseScorer.west);
						\draw[->,line width=.7mm]
							(machineLearningModels.east) --
							(machineLearningModels-|parseScorer.west);
						\draw[->,line width=.7mm]
							(parseScorer.south) -- (parseScorer|-abstractSyntaxTree.north);
						\draw[->,line width=.7mm]
							(parseTree.west) --
							(parseTree-|semanticsGenerator.east);
						\draw[->,line width=.7mm]
							(semanticsGenerator.west) --
							(semanticsGenerator-|semanticRepresentation.east);
						\draw[->,line width=.7mm]
							(semanticRepresentation.west) --
							(semanticRepresentation-|evaluator.east);
						\draw[->,line width=.7mm]
							(evaluator.west) -- (meaningRepresentation.east);
					\end{tikzpicture}
				}
			\end{figure}
			\textcolor{blue!20}{$\blacksquare$}: Domain specific, external knowledge
		\end{frame}

%----------------------------------------

		\subsection{Components}
		\begin{frame}[fragile]
			\frametitle{Syntactic parser}

			\begin{block}{Combinatory categorial grammars}
				A grammar formalism which associates lexical items with syntactic categories.\\
				They are usually represented by context-free grammars in Chomsky normal form.
			\end{block}

			\pause

			\begin{columns}
				\begin{column}{0.5\textwidth}
					\begin{figure}[ht]
						\begin{mylisting}
Rule("$ROOT", "$PandasQuery",
     ProxySemantic(0)),
Rule("$PandasQuery",
     "$OperationWithArguments",
     CombineSemantic([0], ...),
Rule("$OperationWithArguments",
	   "$Operation $OperationArgument",
	   CombineSemantic([0, 1])),
Rule("$Operation",
	   "$SelectOperation",
	   ProxySemantic(0)),
Rule("$Select", "select"),
...
						\end{mylisting}
					\end{figure}
				\end{column}
				\begin{column}{0.5\textwidth}
					\pause

					\begin{figure}[ht]
						\resizebox{\textwidth}{!}{
							\begin{tikzpicture}
								\node[draw, align=center]
									(root)
									at (0, 0)
									{\$ROOT};
		
								\node[draw, align=center]
									(pandasQuery)
									at (0, -1)
									{\$PandasQuery};
		
								\node[draw, align=center]
									(operationWithArguments1)
									at (0, -2)
									{\$OperationWithArguments};
		
								\node[draw, align=center]
									(operationWithArguments2)
									at (-4.5, -3)
									{\$OperationWithArguments};
								\node[draw, align=center]
									(operationArgument1)
									at (4.5, -3)
									{\$OperationArgument};
		
								\node[draw, align=center]
									(operation)
									at (-7.5, -4)
									{\$Operation};
								\node[draw, align=center]
									(operationArgument2)
									at (-3, -4)
									{\$OperationArgument};
								\node[draw, align=center]
									(selectSourceArgument)
									at (4.5, -4)
									{\$SelectSourceArgument};
		
								\node[draw, align=center]
									(selectOperation)
									at (-7.5, -5)
									{\$SelectOperation};
								\node[draw, align=center]
									(selectTypeArgument)
									at (-3, -5)
									{\$SelectTypeArgument};
								\node[draw, align=center]
									(from)
									at (1.5, -5)
									{\$From};
								\node[draw, align=center]
									(namedTypeArgument)
									at (6, -5)
									{\$NamedTypeArgument};
		
								\node[draw, align=center]
									(select)
									at (-7.5, -6)
									{\$Select};
								\node[draw, align=center]
									(column)
									at (-4.5, -6)
									{\$Column};
								\node[draw, align=center]
									(variable1)
									at (-1.5, -6)
									{\$Variable};
								\node[draw, align=center]
									(typeArgument)
									at (4.5, -6)
									{\$TypeArgument};
								\node[draw, align=center]
									(variable2)
									at (7.5, -6)
									{\$Variable};
								
								\node[draw, align=center]
									(token1)
									at (-1.5, -7)
									{\$Token};
								\node[draw, align=center]
									(dataframe)
									at (4.5, -7)
									{\$Dataframe};
								\node[draw, align=center]
									(token2)
									at (7.5, -7)
									{\$Token};
		
								\node[draw, align=center, dashed]
									(selectString)
									at (-7.5, -8)
									{select};
								\node[draw, align=center, dashed]
									(columnString)
									at (-4.5, -8)
									{column};
								\node[draw, align=center, dashed]
									(idString)
									at (-1.5, -8)
									{'id'};
								\node[draw, align=center, dashed]
									(fromString)
									at (1.5, -8)
									{from};
								\node[draw, align=center, dashed]
									(dataframeString)
									at (4.5, -8)
									{dataframe};
								\node[draw, align=center, dashed]
									(dfString)
									at (7.5, -8)
									{df};
		
								\draw[-] (root.south) --
									(pandasQuery.north);
								\draw[-] (pandasQuery.south) --
									(operationWithArguments1.north);
								\draw[-] (operationWithArguments1.south) --
									(operationWithArguments2.north);
								\draw[-] (operationWithArguments2.south) --
									(operation.north);
								\draw[-] (operation.south) --
									(selectOperation.north);
								\draw[-] (selectOperation.south) --
									(select.north);
								\draw[-] (select.south) --
									(selectString.north);
								\draw[-] (operationWithArguments2.south) --
									(operationArgument2.north);
								\draw[-] (operationArgument2.south) --
									(selectTypeArgument.north);
								\draw[-] (selectTypeArgument.south) --
									(column.north);
								\draw[-] (column.south) --
									(columnString.north);
								\draw[-] (selectTypeArgument.south) --
									(variable1.north);
								\draw[-] (variable1.south) --
									(token1.north);
								\draw[-] (token1.south) --
									(idString.north);
								\draw[-] (operationWithArguments1.south) --
									(operationArgument1.north);
								\draw[-] (operationArgument1.south) --
									(selectSourceArgument.north);
								\draw[-] (selectSourceArgument.south) --
									(from.north);
								\draw[-] (from.south) --
									(fromString.north);
								\draw[-] (selectSourceArgument.south) --
									(namedTypeArgument.north);
								\draw[-] (namedTypeArgument.south) --
									(typeArgument.north);
								\draw[-] (typeArgument.south) --
									(dataframe.north);
								\draw[-] (dataframe.south) --
									(dataframeString.north);
								\draw[-] (namedTypeArgument.south) --
									(variable2.north);
								\draw[-] (variable2.south) --
									(token2.north);
								\draw[-] (token2.south) --
									(dfString.north);
							\end{tikzpicture}
						}
					\end{figure}
				\end{column}
			\end{columns}
		\end{frame}

%----------------------------------------

		\begin{frame}
			\frametitle{Semantics}
			\begin{figure}[ht]
				\resizebox{.9\textwidth}{!}{
					\begin{tikzpicture}
						\onslide<1->{
							% category parse tree
							\node[draw, align=center, minimum height=0.5cm, minimum width=1cm]
								(ellipses1)
								at (-6, 5)
								{...};

							\node[draw, align=center]
								(selectTypeArgument)
								at (-6, 0)
								{\$SelectTypeArgument};

							\node[draw, align=center]
								(column)
								at (-7.5, -3)
								{\$Column};
							\node[draw, align=center]
								(variable1)
								at (-4.5, -3)
								{\$Variable};

							\node[draw, align=center]
								(token1)
								at (-4.5, -6)
								{\$Token};

							\node[draw, align=center, dashed]
								(columnString)
								at (-7.5, -7.5)
								{column};
							\node[draw, align=center, dashed]
								(idString)
								at (-4.5, -7.5)
								{'id'};

							\node[align=center]
								()
								at (-6, -9)
								{\LARGE\textbf{Parse tree}};

							\draw[-] (ellipses1.south) --
								(selectTypeArgument.north);
							\draw[-] (selectTypeArgument.south) --
								(column.north);
							\draw[-] (column.south) --
								(columnString.north);
							\draw[-] (selectTypeArgument.south) --
								(variable1.north);
							\draw[-] (variable1.south) --
								(token1.north);
							\draw[-] (token1.south) --
								(idString.north);
						}

						\onslide<2->{
							\draw[->, line width=1mm] (-2.5, -2) -- (1, -2);

							% semantics parse tree
							\node[draw, align=center, minimum height=0.5cm, minimum width=1cm]
								(ellipses2)
								at (5.5, 5)
								{...};

							\node[draw, align=left, rounded corners=2mm, text width=8cm]
								(selectTypeDict)
								at (5.5, 2)
								{\texttt{
									\{\\
										\quad 'selectType': 'column', \\
										\quad 'columnName': \{'variable': 'id' \}\\
									\}
								}};

							\node[draw, align=center]
								(nestedCombineSemantic)
								at (5.5, 0)
								{NestedCombineSemantic};

							\node[draw, align=center, rounded corners=2mm]
								(emptyDict)
								at (3.5, -1.5)
								{\texttt{\{\}}};
							\node[draw, align=center, rounded corners=2mm]
								(variableDict)
								at (7.5, -1.5)
								{\texttt{\{ 'variable': 'id' \}}};

							\node[draw, align=center]
								(emptySemantic)
								at (3.5, -3)
								{EmptySemantic};
							\node[draw, align=center]
								(customSemantic)
								at (7.5, -3)
								{CustomSemantic};

							\node[draw, align=center, rounded corners=2mm]
								(tokenDict)
								at (7.5, -4.5)
								{\texttt{\{ 'token': 'id' \}}};

							\node[draw, align=center]
								(tokenAnnotator)
								at (7.5, -6)
								{TokenAnnotator};

							\node[draw, align=center, dashed]
								(columnString2)
								at (3.5, -7.5)
								{column};
							\node[draw, align=center, dashed]
								(idString2)
								at (7.5, -7.5)
								{'id'};

							\node[align=center]
								()
								at (5.5, -9)
								{\LARGE\textbf{Semantics tree}};

							\draw[-] (ellipses2.south) --
								(selectTypeDict.north);
							\draw[-] (selectTypeDict.south) --
								(nestedCombineSemantic.north);
							\draw[-] (nestedCombineSemantic.south) --
								(emptyDict.north);
							\draw[-] (nestedCombineSemantic.south) --
								(variableDict.north);
							\draw[-] (emptyDict.south) --
								(emptySemantic.north);
							\draw[-] (emptySemantic.south) --
								(columnString2.north);
							\draw[-] (variableDict.south) --
								(customSemantic.north);
							\draw[-] (customSemantic.south) --
								(tokenDict.north);
							\draw[-] (tokenDict.south) --
								(tokenAnnotator.north);
							\draw[-] (tokenAnnotator.south) --
								(idString2.north);
						}

						\onslide<3->{
							\draw[->, line width=1mm] (10.75, -2) -- (14.25, -2);

							% generated semantic representation
							\node[draw, align=left, rounded corners=4mm,
								text width=8cm]
								(outputDict)
								at (19, -2)
								{
									\texttt{\{\\
										\quad 'domain': 'pandas',\\
										\quad 'operation': 'select',\\
										\quad 'selectType': 'column',\\
										\quad 'columnName': \{'variable': '"id"'\},\\
										\quad 'selectSource': \{\\ 
										\quad \quad 'typeVariable': \{'variable': 'df'\},\\
										\quad \quad 'type': 'DataFrame'\\
										\quad\}\\
									\}}
								};

							\node[align=center]
								()
								at (19, -9)
								{\LARGE\textbf{Output semantics}};
						}
					\end{tikzpicture}
				}
			\end{figure}
		\end{frame}

% %----------------------------------------

% 		\begin{frame}
% 			\frametitle{Parse scorer}
% 			\begin{figure}[ht]
% 				\resizebox{\textwidth}{!}{
% 					\begin{tikzpicture}
% 						\node[draw, align=center, minimum height=5.5cm, dashed,
% 							minimum width=15cm]
% 							(parseScorerOutline)
% 							at (10, -1.5)
% 							{};
% 						\node[align=center, above=2mm, anchor=west]
% 							(parseScorerLabel)
% 							at (parseScorerOutline.north west)
% 							{Parse scorer};
% 						\node[draw, align=center, minimum height=2cm, dashed,
% 							minimum width=14.5cm]
% 							(mlPipelineOutline)
% 							at (10, -3)
% 							{};
% 						\node[align=center, above=2mm, anchor=west]
% 							(mlPipelineOutline)
% 							at (mlPipelineOutline.north west)
% 							{Machine learning pipeline};

% 						\node[draw, align=center, minimum height=1.5cm,
% 							minimum width=4cm, rounded corners=4mm]
% 							(parseTree)
% 							at (0, 0)
% 							{
% 								\textbf{Parse tree}
% 							};
% 						\node[draw, align=center, minimum height=1.5cm, text width=4cm,
% 							minimum width=4cm]
% 							(generateFeatureVector)
% 							at (10, 0)
% 							{\textbf{Generate \\ feature vector}};
% 						\node[draw, align=center, minimum height=1.5cm,
% 							minimum width=4cm, rounded corners=4mm]
% 							(featureVector)
% 							at (15, 0)
% 							{\textbf{Feature vector}};
% 						\node[draw, align=center, minimum height=1.5cm,
% 							minimum width=4cm]
% 							(scaler)
% 							at (15, -3)
% 							{\textbf{Scaler}};
% 						\node[draw, align=center, minimum height=1.5cm,
% 							minimum width=4cm]
% 							(reducer)
% 							at (10, -3)
% 							{\textbf{Reducer}};
% 						\node[draw, align=center, minimum height=1.5cm,
% 							minimum width=4cm]
% 							(regressionModel)
% 							at (5, -3)
% 							{\textbf{Regression model}};
% 						\node[draw, align=center, minimum height=1.5cm,
% 							minimum width=4cm, rounded corners=4mm]
% 							(score)
% 							at (0, -3)
% 							{\textbf{Score}};
						
% 						\draw[->, line width=.7mm]
% 							(parseTree.east) --
% 							(parseTree-|generateFeatureVector.west);
% 						\draw[->, line width=.7mm]
% 							(generateFeatureVector.east) --
% 							(generateFeatureVector-|featureVector.west);
% 						\draw[->, line width=.7mm]
% 							(featureVector.south) --
% 							(featureVector|-scaler.north);
% 						\draw[->, line width=.7mm]
% 							(scaler.west) --
% 							(scaler-|reducer.east);
% 						\draw[->, line width=.7mm]
% 							(reducer.west) --
% 							(reducer-|regressionModel.east);
% 						\draw[->, line width=.7mm]
% 							(regressionModel.west) --
% 							(regressionModel-|score.east);
% 					\end{tikzpicture}
% 				}
% 			\end{figure}
% 		\end{frame}

% %----------------------------------------

% 		\begin{frame}
% 			\frametitle{Feature engineering}
% 			\begin{figure}[ht]
% 				\resizebox{\textwidth}{!}{
% 					\begin{tikzpicture}
% 						\node[draw, align=center, minimum width=1cm, minimum height=.5cm]
% 							(rule1)
% 							at (0, 0)
% 							{Rule 1};
% 						\node[draw, align=center, minimum width=1cm, minimum height=.5cm]
% 							(ellipses1)
% 							at (1, -1)
% 							{...};
% 						\node[draw, align=center, minimum width=1cm, minimum height=.5cm]
% 							(rule2)
% 							at (-1, -1)
% 							{Rule 2};
% 						\node[draw, align=center, minimum width=1cm, minimum height=.5cm]
% 							(ellipses2)
% 							at (-1, -2)
% 							{...};

% 						\draw[-] (rule1.south) -- (ellipses1.north);
% 						\draw[-] (rule1.south) -- (rule2.north);
% 						\draw[-] (rule2.south) -- (ellipses2.north);

% 						\draw[->, line width=.7mm]
% 							(1.75, -1) -- (3, -1);

% 						\node[align=center] () at (6, -1) {
% 							$
% 							\begin{bmatrix}
% 								\# Occurences Rule 1 \\
% 								\# Occurences Rule 2 \\
% 								\vdots \\
% 								\# Occurences Rule N \\
% 								\# Ocurrences Rule 1 Before Rule 1 \\
% 								\# Ocurrences Rule 1 Before Rule 2 \\
% 								\vdots \\
% 								\# Occurences Rule N Before Rule N\\
% 							\end{bmatrix}
% 							$
% 						};

% 						\draw[->, line width=.7mm]
% 							(8.75, -1) -- (10, -1);

% 						\node[align=center] () at (10.5, -1) {
% 							$
% 							\begin{bmatrix}
% 								1 \\
% 								1 \\
% 								\vdots \\
% 								0 \\
% 								0 \\
% 								1 \\
% 								\vdots \\
% 								0
% 							\end{bmatrix}
% 							$
% 						};
						
% 						% \draw[->, line width=.7mm]
% 						% 	(regressionModel.west) --
% 						% 	(regressionModel-|score.east);
% 					\end{tikzpicture}
% 				}
% 			\end{figure}
% 		\end{frame}

% %----------------------------------------

% 		\begin{frame}
% 			\frametitle{Evaluator}
% 			\begin{figure}[ht]
% 				\resizebox{0.8\textwidth}{!}{
% 					\begin{tikzpicture}
% 						\node[draw, align=left, minimum height=5cm, rounded corners=4mm,
% 							text width=8cm, minimum width=8cm]
% 							(semanticRepresentation)
% 							at (0, 0)
% 							{
% 								\begin{center} 
% 									\vskip-1em
% 									\textbf{Semantic representation}
% 								\end{center}
% 								\vskip-1em
% 								\small
% 								\texttt{\{\\
% 									\quad 'domain': 'pandas',\\
% 									\quad 'operation': 'select',\\
% 									\quad 'selectType': 'column',\\
% 									\quad 'columnName': \{'variable': '"id"'\},\\
% 									\quad 'selectSource': \{\\ 
% 									\quad \quad 'typeVariable': \{'variable': 'df'\},\\
% 									\quad \quad 'type': 'DataFrame'\\
% 									\quad\}\\
% 								\}}
% 							};
% 						\node[draw, align=center, minimum height=5cm,
% 							text width=3cm, minimum width=3cm]
% 							(evaluator)
% 							at (7, 0)
% 							{\textbf{Evaluator}};
% 						\node[draw, align=center, minimum height=5cm, rounded corners=4mm,
% 							text width=3cm, minimum width=3cm]
% 							(meaningRepresentation)
% 							at (12, 0)
% 							{
% 								\textbf{Meaning representation}\\
% 								\texttt{df[’id’]}
% 							};
						
% 						\draw[->, line width=.7mm]
% 							(semanticRepresentation.east) --
% 							(semanticRepresentation-|evaluator.west);
% 						\draw[->, line width=.7mm]
% 							(evaluator.east) --
% 							(evaluator-|meaningRepresentation.west);
% 					\end{tikzpicture}
% 				}
% 			\end{figure}
			
% 			\pause
% 			\vfill
			
% 			\textbf{Possible operations:}
% 			\begin{multicols}{3}
% 				\begin{itemize}
% 					\item create
% 					\item select rows
% 					\item select column
% 					\item select aggregated column
% 					\item convert
% 					\item append
% 					\item sort
% 				\end{itemize}
% 			  \end{multicols}
% 		\end{frame}

%----------------------------------------

		\begin{frame}
			\frametitle{Visual Studio Code extension}
			\begin{figure}[ht]
				\begin{center}
					\resizebox{\textwidth}{!}{
						\begin{tikzpicture}
							\node[minimum height=7cm, minimum width=9cm, draw,
								dashed, rounded corners]
								(vscodeoutline) at (0, 0)
								{};
							\node[above] () at (vscodeoutline.north) {Visual Studio Code};
	
							\node[minimum height=7cm, minimum width=6cm, draw,
								dashed, rounded corners]
								(languageserveroutline) at (10, 0)
								{};
							\node[above] () at (languageserveroutline.north)
								{Language server};
	
							\node[minimum height=8.5cm, minimum width=13.5cm, draw,
								dashed, rounded corners]
							(pythonpackage) at (13.5, .25)
							{};
							\node[above] () at (pythonpackage.north) {Python package};
	
	
							\node[text width=2.5cm, align=center, minimum height=6cm,
								minimum width=2.5cm, draw] (sourcefile)
								at (-2.5, 0)
								{Source file};
							\node[text width=2.5cm, align=center, minimum height=6cm,
								minimum width=2.5cm, draw] (languageclient)
								at (2.5, 0)
								{Language client};
	
							\node[text width=5cm, align=center, minimum height=1cm,
								minimum width=2cm, draw] (generateparsetrees)
								at (10, 2.25)
								{Generate parse trees and semantics};
							\node[text width=5cm, align=center, minimum height=1cm,
								minimum width=2cm, draw] (scoreparsetrees)
								at (10, 0)
								{Score parse trees by likeliness};
							\node[text width=5cm, align=center, minimum height=1cm,
								minimum width=2cm, draw] (generatesnippet)
								at (10, -2.25)
								{Generate snippet from best parse tree};
	
							\node[text width=4cm, align=center, minimum height=1cm,
								minimum width=2cm, draw] (grammar)
								at (17.5, 2.25)
								{CCG Grammar};
							\node[text width=4cm, align=center, minimum height=1cm,
								minimum width=2cm, draw] (mlmodel)
								at (17.5, 0)
								{ML Models};
							\node[text width=4cm, align=center, minimum height=1cm,
								minimum width=2cm, draw] (evaluator)
								at (17.5, -2.25)
								{Evaluator};
	
							\draw[->, line width=.3mm]
								([yshift=2.25cm]sourcefile.east)
								-- node [text width=2cm, align=center, midway, above]
									{\small Read \linebreak utterance}
								([yshift=2.25cm]languageclient.west);
							\draw[->, line width=.3mm]
								([yshift=-2.25cm]languageclient.west)
								-- node [midway, above] {\small Insert snippet}
								([yshift=-2.25cm]sourcefile.east);
							\draw[->, line width=.3mm]
								([yshift=2.25cm]languageclient.east)
								-- node [text width=2cm, align=center, midway, above]
									{Language \linebreak server \linebreak protocol}
								(generateparsetrees.west);
							\draw[->, line width=.3mm]
								(generatesnippet.west) --
								([yshift=-2.25cm]languageclient.east);
							\draw[->, line width=.3mm]
								(generateparsetrees.south) --
								(scoreparsetrees.north);
							\draw[->, line width=.3mm]
								(scoreparsetrees.south) --
								(generatesnippet.north);
							\draw[->, line width=.3mm]
								(grammar.west) --
								(generateparsetrees.east);
							\draw[->, line width=.3mm]
								(mlmodel.west) --
								(scoreparsetrees.east);
							\draw[->, line width=.3mm]
								(evaluator.west) --
								(generatesnippet.east);
						\end{tikzpicture}
					}
				\end{center}
			\end{figure}
		\end{frame}

%----------------------------------------

	\section{Results}
		\subsection{Machine learning models}
		\begin{frame}
			\frametitle{Comparison of machine learning models}
			Variables
			\begin{itemize}
				\item Scaler type
				\item Reducer type
				\item Regression model type
				\item Reduced feature count
			\end{itemize}

			\pause

			\vskip-.5em
			\begin{table}
				\begin{center}
					\begin{tabular}{l|c}
						Regression model & $R^2$-score$_{max}$ \\ \hline % & scaler & reducer & \# reduced feactures \\ \hline
						\texttt{DecisionTreeRegressor} & 0.57 \\ % & None & FeatureAgglomeration & 30 \\
						\texttt{GradientBoostingRegressor} & 0.59 \\ % & StandardScaler & SparseRandomProjection & 500 \\
						\texttt{KNeighborsRegressor} & 0.62 \\ % & None & FeatureAgglomeration & 500 \\
						\texttt{MLPRegressor} & 0.66 \\ % & MaxAbsScaler & SparseRandomProjection & 500 \\
						\texttt{RandomForestRegressor} & 0.61 \\ % & None & FeatureAgglomeration & 30 \\
						\texttt{SVR} & 0.58 \\ % & None & PCA & 500
					\end{tabular}
				\end{center}
			\end{table}
		\end{frame}

%----------------------------------------

		\subsection{Comparison to TRANX}
		\begin{frame}
			\frametitle{Comparison to TRANX}
			\textbf{Example} ("sort df by col1, col2"):

			\begin{table}[H]
				\begin{center}
					\begin{tabular}{l|c}
						Parser & Snippet \\ \hline
						Expected & \texttt{df.sort\_values([col1,col2])} \\
						CCG & \texttt{df.sort\_values([col1,col2])} \\
						TRANX$_{pandas}$ & \texttt{var0.<unk>()} \\
						TRANX$_{pandas\&conala}$ & \texttt{var0.sort\_values([var1,var2, var3])}
					\end{tabular}
				\end{center}
			\end{table}

			\pause

			\begin{table}[H]
				\begin{center}
					\begin{tabular}{l|c|c|c}
						Parser & ED$_{mean}$ & ED$_{min}$ & ED$_{max}$ \\ \hline
						Combinatory categorial grammar & 2.3 & 0 & 10.5 \\
						TRANX$_{pandas}$ & 7.1 & 4.9 & 21.6 \\
						TRANX$_{pandas \& conala}$ & 3.4 & 2.2 & 30.3 \\
						TRANX$_{pandas}$ (deanonymized) & 7.0 & 5.9 & 17.1 \\
						TRANX$_{pandas \& conala}$ (deanonymized) & 2.7 & 2.1 & 21.2
					\end{tabular}
				\end{center}
			\end{table}
		\end{frame}

%----------------------------------------

		\subsection{Improvements}
		\begin{frame}
			\frametitle{Improvements - Out of vocabulary tokens}
			\begin{block}{Word embeddings}
				Representation of words as vectors of real numbers of a given dimension.\\
				Here, distance of vectors represents similarity (synonymity) of corresponding
				words.
			\end{block}

			\pause

			\begin{table}[H]
				\begin{center}
					\begin{tabular}{l|c|c|c|c}
						Similarity threshold & ED$_{mean}$ & ED$_{min}$ & ED$_{max}$
							& \#parses$_{mean}$ \\ \hline
						1.0 & 2.3 & 0 & 10.5 & 85.7 \\
						0.9 & 2.3 & 0.9 & 9.9 & 89.3 \\
						0.8 & 3.8 & 1.8 & 7.6 & 153.9 \\
						0.7 & 4.0 & 1.8 & 8.0 & 386.6 \\
						0.6 & 14.0 & 4.2 & 15.1 & 1883.6 \\
						0.5 & 16.9 & 15.2 & 18.5 & 4590.7
					\end{tabular}
				\end{center}
			\end{table}

			Used word embedding: spaCy \texttt{en\_core\_web\_md}
		\end{frame}

%----------------------------------------

	\section{Conclusion}
		\subsection{Summary}
		\begin{frame}
			\frametitle{Summary}
			\textbf{Key points}
			\begin{itemize}
				\item Grammar deduction is difficult
				\item Similar capabilities to TRANX, input/output domain are more specific
				\item Many machine learning models perform well, MLPRegressor performs best
				\item Vulnerable to out of vocabulary tokens
			\end{itemize}

			\pause

			\textbf{Outlook}
			\begin{itemize}
				\item Word embeddings
				\item Translation
				\item Automatic grammar deduction
				\item General purpose Python package
			\end{itemize}
		\end{frame}

%----------------------------------------

	\section{Demonstration}
		\begin{frame}
			\begin{center}
				\includegraphics[width=.8\textwidth]{vscode-extension-auto-completion-example.png}
				\includegraphics[width=.8\textwidth]{vscode-extension-code-action-example.png}
			\end{center}
		\end{frame}

%----------------------------------------

		\begin{frame}
			\begin{center}
				\includegraphics[width=.8\textwidth]{vscode-extension-inserted-snippet-example.png}
			\end{center}
		\end{frame}

\end{document}
