Welcome to Semantic Parsing's documentation!
============================================

.. automodule:: semantic_parsing
   :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

