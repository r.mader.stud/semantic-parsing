import editdistance as ed

conalaPandasData = [
    ("mycsv = df.to_csv()", [
        "df = pd.DataFrame(df)",
        "<unk> = df.concat(var_1)",
        "<unk> = df.<unk>()",
        "df.to_csv('mycsv')",
        "<unk> = df.<unk>('mycsv')"
    ]),
    ("df.to_csv()", [
        "df.to_csv()",
        "df.concat(var_1)",
        "df.append([[var_1 for var_2 in var_3] for var_2 in var_3])",
        "df",
        ""
    ]),
    ("mean = df[col].mean()", [
        "min = df[col].max()",
        "result = df[col].max()",
        "min = df.groupby([col, var_2])[col].mean()",
        "min = df[col].min()",
        "new_df = df[col].max()"
    ]),
    ("df[{column}]", [
        "df[{column}]",
        "df[[str_0, str_1]]",
        "{df}[{column}]",
        "df[{'column'}]",
        ""
    ]),
    ("df[col].mean()", [
        "df[col].mean()",
        "df[col].max()",
        "max = df[col].max()",
        "df.mean()",
        ""
    ]),
    ("concat([df1, df2])", [
        "concat([df1, df2])",
        "concat([df1, df2], axis=str_0)",
        "df1(df1, df2)",
        "var_0",
        ""
    ]),
    ("min = df[col].min()", [
        "df[col].min()",
        "df[col].max()",
        "df[col].sum()",
        "df.sort(col)",
        ""
    ]),
    ("df[var1 > var2]", [
        "df[var1 > var2]",
        "df[list(var1)]",
        "df",
        ""
    ]),
    ("df[col].mean()", [
        "df[col].mean()",
        "df[col].legend()",
        "df[col].loc(col[col])",
        "df[col].plot()",
        ""
    ]),
    ("result = {dataframe}.mean()", [
        "result = {dataframe}.mean()",
        "result = dataframe.mean()",
        "result = {dataframe}[var_1]",
        "result = {dataframe}.mean()\n"
        "result = dataframe.mean()",
        "result = {dataframe}.values"
    ])
]
purePandasData = [
    ("mycsv = df.to_csv()", [
        "<unk> = df.<unk>()",
        "result = df.<unk>()",
        "<unk> = df(<unk>)",
        "<unk> = df.<unk>('mycsv')",
        ""
    ]),
    ("df.to_csv()", [
        "df.<unk>()",
        "<unk> = df.<unk>()",
        "result = df.<unk>()",
        "df.<unk>(var_1)",
        ""
    ]),
    ("mean = df[col].mean()", [
        "<unk> = df[col].<unk>()",
        "result = df[col].<unk>()",
        "<unk> = df[col].min()",
        "<unk> = {df}.<unk>()",
        ""
    ]),
    ("df[{column}]", [
        "df[{column}]",
        "{df}[{column}]",
        "df[column]",
        "{df}[column]",
        ""
    ]),
    ("df[col].mean()", [
        "df[col].<unk>()",
        "df[col].min()",
        "{df}.<unk>()",
        "df.<unk>()",
        ""
    ]),
    ("concat([df1, df2])", [
        "df1([df1, var_2])",
        "result = df1([df1, var_2])",
        "result = df1.<unk>()",
        "result = df1()",
        ""
    ]),
    ("min = df[col].min()", [
        "result = df[col].<unk>()",
        "<unk> = df[col].<unk>()",
        "<unk> = {df}.<unk>()",
        "result = {df}.<unk>()",
    ]),
    ("df[var1 > var2]", [
        "df[[var1]]",
        "{df}[{var1}]",
        "df[{var1}]",
        "{df}['var1']",
        ""
    ]),
    ("df[col].mean()", [
        "df[col].<unk>()",
        "{df}.<unk>()",
        "df[col].min()",
        "df.<unk>()",
        ""
    ]),
    ("result = {dataframe}.mean()", [
        "result = {dataframe}.<unk>()",
        "result = dataframe[var_1].<unk>()",
        "result = dataframe([var_1, var_2])",
        "result = dataframe.<unk>()",
        ""
    ])
]


def mean(data):
    return sum(data) / len(data)


bestParseEditDistances = []
minEditDistances = []
maxEditDistances = []

for expectedSnippet, evaluatedSnippets in purePandasData:

    editDistances = []

    for evaluatedSnippet in evaluatedSnippets:
        editDistance = ed.eval(expectedSnippet, evaluatedSnippet)
        editDistances.append(editDistance)

    bestParseEditDistance = editDistances[0]
    minEditDistance = min(editDistances)
    maxEditDistance = max(editDistances)

    bestParseEditDistances.append(bestParseEditDistance)
    minEditDistances.append(minEditDistance)
    maxEditDistances.append(maxEditDistance)

meanBestParseEditDistance = mean(bestParseEditDistances)
meanMinEditDistance = mean(minEditDistances)
meanMaxEditDistance = mean(maxEditDistances)

print(
    f"Mean best parse ED: {meanBestParseEditDistance}; "
    f"Mean min ED: {meanMinEditDistance}; "
    f"Mean max ED: {meanMaxEditDistance}"
)
