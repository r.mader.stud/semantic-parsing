# script for baking all models and data used for experimentation
python -m semantic_pandas.scripts.bake_data sm 10
python -m semantic_pandas.scripts.bake_data md 30
python -m semantic_pandas.scripts.bake_data lg 100
python -m semantic_pandas.scripts.bake_data xl 500
