# script for automatically installing the the repository
# NOTE: This script requires:
# - A working python3 installation with pip (if you wish for the repository to use a virtual
#   environment, make sure to activate before execution)
# - A working npm installation

function PromptConfirmation($Message) {
    Write-Host $Message

    $reply = ""
    while ($True) {
        $reply = Read-Host

        if ($reply -match "[yYnN]") {
            break
        }

        Write-Host "Input not understood"
    }

    if ($reply -match "[yY]") {
        return $True
    } else {
        return $False
    }
}


# install python package
if (PromptConfirmation "Install python package? (y/n)") {
    if (PromptConfirmation "Install in dev mode? (y/n)") {
        Write-Host "Installing python package in dev mode..."
        python -m pip install -e $PSScriptRoot/..[dev]
    } else {
        Write-Host "Installing python package..."
        python -m pip install $PSScriptRoot/..
    }
}

# install language client
if (PromptConfirmation "Install language client? (y/n)") {
    Write-Host "Installing language client..."
    Set-Location $PSScriptRoot/../language_client
    npm i

    if (PromptConfirmation "Generate .vsix language client extension file? (y/n)") {
        Write-Host "Packaging language client extension..."
        Set-Location $PSScriptRoot/../language_client
        mkdir dist
        npm run package
    }
}
