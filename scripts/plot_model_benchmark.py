"""Creates heatmap plots to be used in the thesis and saves them in the misc/thesis/img dir."""

import csv
from os import path

import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

SCRIPT_ROOT = path.dirname(path.realpath(__file__))
SAVE_DESTINATION = path.join(SCRIPT_ROOT, "../misc/thesis/img")
BENCHMARK_TYPE = "training"  # must be either "testing" or "training"
PLOT_FONT_SIZE = 20

SIZE_LABEL_TO_FEATURE_COUNT = {
    "sm": "10",
    "md": "30",
    "lg": "100",
    "xl": "500"
}

SCALER_LABEL_TO_DISPLAY_NAME = {
    "none": "None",
    "standard": "SS",  # StandardScaler
    "maxabs": "MAS"  # MaxAbsScaler
}

REDUCER_LABEL_TO_DISPLAY_NAME = {
    "pca": "PCA",  # PCA
    "featureagg": "FA",  # FeatureAgglomeration
    "univariate": "GUS",  # GenericUnivariateSelect
    "sparserandom": "SRP"  # SparseRandomProjection
}

MODEL_LABEL_TO_DISPLAY_NAME = {
    "gradientboosting": "GBR",  # GradientBoostingRegressor
    "kneighbors": "KNR",  # KNeighborsRegressor
    "perceptron": "MLPR",  # MLPRegressor
    "svr": "SVR",  # SVR
    "tree": "DTR",  # DecisionTreeRegressor
    "forest": "RFR"  # RandomForestRegressor
}

SCALER_LABEL_ORDER = [
    "None",
    "SS",
    "MAS"
]

REDUCER_LABEL_ORDER = [
    "PCA",
    "FA",
    "GUS",
    "SRP"
]


RESULT_FILE = f"resources/{BENCHMARK_TYPE}_benchmark_results.csv"
DELIMITER = ";"

plotModelLabels = set()
plotScalerReducerLabels = set()


def getScalerReducerLabelSortingKey(scalerReducerLabel: str):
    scaler, reducer = scalerReducerLabel.split("/")[:2]

    try:
        scalerIndex = SCALER_LABEL_ORDER.index(scaler)
    except ValueError:
        scalerIndex = float("inf")

    try:
        reducerIndex = REDUCER_LABEL_ORDER.index(reducer)
    except ValueError:
        reducerIndex = float("inf")

    return (scalerIndex, reducerIndex)


def plotAndSaveResults(results, sizeLabel):
    # set overall fontsize
    plt.rc(
        "font",
        size=PLOT_FONT_SIZE
    )

    # create plot object
    fig, ax = plt.subplots(figsize=(18, 10))

    # plot data as heatmap
    colorMap = LinearSegmentedColormap.from_list("customColorMap", ["white", "skyblue", "maroon"])
    im = ax.imshow(results, cmap=colorMap, vmin=-1, vmax=1)
    fig.colorbar(im, ax=ax, fraction=0.02313)

    # set tick values
    ax.set_xticks(range(len((plotScalerReducerLabels))), )
    ax.set_yticks(range(len(plotModelLabels)))

    # set tick labels
    ax.set_xticklabels(sorted(plotScalerReducerLabels, key=getScalerReducerLabelSortingKey))
    ax.set_yticklabels(sorted(plotModelLabels))

    # rotate tick labels
    plt.setp(
        ax.get_xticklabels(),
        rotation=45,
        ha="right",
        rotation_mode="anchor",
        fontsize=PLOT_FONT_SIZE
    )

    # add text annotation to heatmap cells
    for i in range(len(plotModelLabels)):
        localMax = max(results[i])
        for j in range(len(plotScalerReducerLabels)):
            score = results[i][j]
            textColor = "black" if score < 0.75 else "white"
            fontweight = "bold" if score == localMax else "normal"
            ax.text(
                j, i, f"{round(results[i][j], 2):.2f}",
                ha="center", va="center",
                color=textColor, fontweight=fontweight, fontsize=PLOT_FONT_SIZE
            )

    # set misc labels
    # ax.set_title(
    #     f"Model R² score by scaler-reducer-pair for {BENCHMARK_TYPE} dataset "
    #     f"(reduced features={SIZE_LABEL_TO_FEATURE_COUNT[sizeLabel]})"
    # )
    ax.set_ylabel("Model type", fontdict={"fontsize": PLOT_FONT_SIZE})
    ax.set_xlabel(
        "Scaler-Reducer pair (as <scaler>/<reducer>)",
        fontdict={"fontsize": PLOT_FONT_SIZE}
    )

    # increase bottom space of plot to prevent text overflow
    # plt.subplots_adjust(bottom=.2)

    # use tight layout for less padded whitespace
    plt.tight_layout()

    # render and save figure
    imageName = f"{BENCHMARK_TYPE}_accuracy_{sizeLabel}.jpg"
    fig.savefig(path.join(SAVE_DESTINATION, imageName), format="jpg")


# read results from csv
results = dict()
with open(RESULT_FILE) as csvFile:
    csvReader = csv.DictReader(csvFile, delimiter=DELIMITER)
    for row in csvReader:
        scalerLabel = SCALER_LABEL_TO_DISPLAY_NAME[str(row["scaler"])]
        reducerLabel = REDUCER_LABEL_TO_DISPLAY_NAME[str(row["reducer"])]
        modelLabel = MODEL_LABEL_TO_DISPLAY_NAME[str(row["model"])]
        sizeLabel = str(row["size"])
        score = float(row["score"])

        scalerReducerLabel = f"{scalerLabel}/{reducerLabel}"

        results.setdefault(sizeLabel, {}) \
               .setdefault(modelLabel, {})[scalerReducerLabel] = score


for sizeLabel, resultsBySize in results.items():
    resultsForPlotByModel = dict()
    plotModelLabels = set()
    plotScalerReducerLabels = set()

    for modelLabel, resultsByModel in sorted(resultsBySize.items(), key=lambda item: item[0]):
        plotModelLabels.add(modelLabel)
        for scalerReducerLabel, score in sorted(
                resultsByModel.items(),
                key=lambda item: getScalerReducerLabelSortingKey(item[0])
                ):
            plotScalerReducerLabels.add(scalerReducerLabel)
            resultsForPlotByModel.setdefault(modelLabel, []).append(score)

    resultsForPlot = [
        scores for modelLabel, scores in sorted(
            resultsForPlotByModel.items(),
            key=lambda item: item[0]
        )
    ]

    plotAndSaveResults(resultsForPlot, sizeLabel)
