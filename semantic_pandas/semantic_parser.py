"""
Class module for the `SemanticParser` class.
Please see `help(SemanticParser)` for more information.
"""

from typing import Callable, Sequence, Tuple

from .grammar import Grammar
from .parse import Parse


class SemanticParser:
    """Utility class for parsing natural language utterances."""

    def __init__(
            self,
            grammar: Grammar,
            scoringFunction: Callable[[Parse], float]):
        """Please see `help(Grammar)` for more information."""
        self.grammar = grammar
        self.scoringFunction = scoringFunction

    def parse(self, utterance: str) -> Parse:
        """Parse the given utterance and return the highest scoring parse."""
        return self.parseAll(utterance)[0]

    def parseAll(self, utterance: str) -> Sequence[Tuple[Parse, float]]:
        """
        Parse the given utterance and return all possible parses.

        Parses are returned in the order of their corresponding score (descending).
        """
        # generate all possible parses
        parses = self.grammar.parse(utterance)

        # add a score to all parses
        scoredParses = []
        for parse in parses:
            score = self.scoringFunction(parse)
            scoredParses.append((parse, score))

        # sort parses by score (descending)
        scoredParses.sort(key=lambda x: x[1], reverse=True)

        return [parseScorePair[0] for parseScorePair in scoredParses]
