"""Script for extracting pandas related queries from the CoNaLa dataset."""

import os
import json

# from ..grammars.pandas_grammar import pandasGrammar
# from ..evaluators import PandasSemanticEvaluator

CONALA_FILE_PATH = os.path.join(os.path.dirname(__file__), "../../resources/conala-mined.jsonl")
OUT_FILE = os.path.join(os.path.dirname(__file__), "../../resources/conala-extracted-queries.txt")

# pandasGrammar.similarityThreshold = 0.8


def isPandasRelated(query):
    """Determine if a given query is related to the pandas domain."""
    containsKeywords = "panda" in query["intent"].lower() or "dataframe" in query["intent"].lower()
    isProbable = float(query["prob"]) > 0.1
    # isTooLong = len(query["intent"]) > 70

    return containsKeywords and isProbable  # and not isTooLong


print("Loading conala dataset...")
with open(CONALA_FILE_PATH, "r") as conalaFile:

    conalaQueries = [json.loads(line) for line in conalaFile.readlines()]
    print(f"Loaded {len(conalaQueries)} queries.")

    print("Extracting pandas related queries...")
    pandasQueries = [
        query for query in conalaQueries if isPandasRelated(query)
    ]
    print(f"Extracted {len(pandasQueries)} pandas related queries.")

# print("Extracting evaluatable queries...")
# evaluator = PandasSemanticEvaluator()
# evaluatableQueries = []
# for i, query in enumerate(pandasQueries):
#     intent = query["intent"]

#     evaluatable = False
#     parses = pandasGrammar.parse(intent)
#     for parse in parses:
#         evaluatedSnippet = evaluator.evaluate(parse)
#         if evaluatedSnippet is not None:
#             evaluatable = True
#             break

#     if evaluatable:
#         evaluatableQueries.append(query)


# print(f"Extracted {len(evaluatableQueries)} evaluatable queries.")

print("Writing to file...")
with open(OUT_FILE, "w+") as outFile:
    for q in pandasQueries:
        # line = f"{q['intent']}; {q['snippet']}"
        # line = line.replace("\n", "")
        line = json.dumps(q)
        outFile.write(line + "\n")

print("Done.")
