"""Benchmark the editting distance of the semantic parser."""

import editdistance as ed

from semantic_pandas import SemanticParser, SklearnParseScorer
from semantic_pandas.evaluators import PandasSemanticEvaluator
from semantic_pandas.grammars import pandasGrammar
from semantic_pandas.utility import loadResource
from semantic_pandas.datasets import testingManualPandasQueries

SIMILARITY_THRESHOLD = 0.6


def mean(data):
    """Calculate the mean of the given sequence of numbers."""
    return sum(data) / len(data)


reducer = loadResource("none_univariate_reducer_lg")
scoringModel = loadResource("none_univariate_perceptron_model_lg")

pandasGrammar.similarityThreshold = SIMILARITY_THRESHOLD

scorer = SklearnParseScorer(
            scoringModel,
            lambda parse: parse.featureVector(pandasGrammar),
            [reducer])

parser = SemanticParser(pandasGrammar, scorer)
evaluator = PandasSemanticEvaluator()

bestParseEditDistances = []
minEditDistances = []
maxEditDistances = []
totalParseCount = 0  # pylint: disable=invalid-name # variable is not a constant

for intent, snippet in testingManualPandasQueries:
    parses = parser.parseAll(intent)
    topParses = parses[:5]

    editDistances = []
    for parse in topParses:
        evaluatedSnippet = evaluator.evaluate(parse)
        if evaluatedSnippet is None:
            evaluatedSnippet = ""  # pylint: disable=invalid-name # variable is not a constant

        editDistance = ed.eval(snippet, evaluatedSnippet)
        editDistances.append(editDistance)

    bestParseEditDistance = editDistances[0]
    minEditDistance = min(editDistances)
    maxEditDistance = max(editDistances)

    totalParseCount += len(parses)
    bestParseEditDistances.append(bestParseEditDistance)
    minEditDistances.append(minEditDistance)
    maxEditDistances.append(maxEditDistance)

    print(
        f"Expected snippet: {snippet}; Evaluated snippet: {evaluatedSnippet}; "
        f"Best parse ED: {bestParseEditDistance}; Min ED: {minEditDistance}; "
        f"Max ED: {maxEditDistance}; "
        f"Parse count: {len(parses)}"
    )

meanBestParseEditDistance = mean(bestParseEditDistances)
meanMinEditDistance = mean(minEditDistances)
meanMaxEditDistance = mean(maxEditDistances)

print("#" * 50)
print(
    f"Mean best parse ED: {meanBestParseEditDistance}; "
    f"Mean min ED: {meanMinEditDistance}; "
    f"Mean max ED: {meanMaxEditDistance}; "
    f"Total parse count: {totalParseCount}; "
    f"Average parse count: {totalParseCount / len(testingManualPandasQueries)}"
)
