"""Script for benchmarking training/testing model accuracy for all models of a given size."""
# NOTE: script could be refactored using smaller functions
# pylint: disable=too-many-locals,too-many-branches,too-many-statements

from collections import defaultdict

from ..utility import loadResource

AS_CSV = True

SIZE_SUFFIXES = ["sm", "md", "lg", "xl"]

SCALER_LABELS = [
    "none",
    "standard",
    "maxabs"
]

REDUCER_LABELS = [
    "sparserandom",
    "featureagg",
    "univariate",
    "pca"
]

MODEL_LABELS = [
    "perceptron",
    "kneighbors",
    "tree",
    "svr",
    "gradientboosting",
    "forest"
]


def runBenchmark(dataResourceName):
    """Run the benchmark."""
    trainingDataset = loadResource(dataResourceName)
    trainingInputVectors = trainingDataset[0].toarray()
    trainingOutputVector = trainingDataset[1].toarray()[0]

    for sizeSuffix in SIZE_SUFFIXES:
        results = defaultdict(dict)
        for scalerLabel in SCALER_LABELS:
            scaler = loadResource(f"{scalerLabel}_scaler")
            if scaler is not None:
                scaledTrainingInputVectors = scaler.transform(trainingInputVectors)
            else:
                scaledTrainingInputVectors = trainingInputVectors

            for reducerLabel in REDUCER_LABELS:
                reducer = loadResource(f"{scalerLabel}_{reducerLabel}_reducer_{sizeSuffix}")
                reducedTrainingInputVectors = reducer.transform(scaledTrainingInputVectors)

                for modelLabel in MODEL_LABELS:
                    model = loadResource(
                        f"{scalerLabel}_{reducerLabel}_{modelLabel}_model_{sizeSuffix}"
                    )

                    score = model.score(reducedTrainingInputVectors, trainingOutputVector)
                    results.setdefault(modelLabel, {}) \
                        .setdefault(reducerLabel, {})[scalerLabel] = score

        if not AS_CSV:
            for modelLabel, modelResults in results.items():
                print(f"{modelLabel}:")
                for reducerLabel, reducerResults in modelResults.items():
                    for scalerLabel, score in reducerResults.items():
                        print(f"\t{scalerLabel}/{reducerLabel}: {score}")
        else:
            for modelLabel, modelResults in results.items():
                for reducerLabel, reducerResults in modelResults.items():
                    for scalerLabel, score in reducerResults.items():
                        print(f"{scalerLabel};{reducerLabel};{modelLabel};{sizeSuffix};{score}")


# benchmark training accuarcy
print("Training data benchmark:")
runBenchmark("pandas_training_data_sparse")
print()

# benchmark test data accuarcy
print("Testing data benchmark:")
runBenchmark("pandas_testing_data_sparse")
