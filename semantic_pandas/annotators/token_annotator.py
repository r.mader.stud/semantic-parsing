"""
Class module for the `TokenAnnotator` class.
Please see `help(TokenAnnotator)` for more information.
"""

from typing import Sequence, Dict, Any

from .annotator import Annotator


class TokenAnnotator(Annotator):
    """
    Annotator for annotating any token.

    This annotator is able to annotate any single token of any kind.
    """
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def annotate(self, tokens: Sequence[str]) -> Dict[str, Any]:
        """
        Return a dictionary representing the semantics of the annotated tokens.

        The returned semantics are a dictionary of the shape:
        {'token': str}
        """
        annotation = None
        if len(tokens) == 1:
            annotation = {"token": tokens[0]}

        return annotation
