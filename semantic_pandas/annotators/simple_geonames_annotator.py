"""
Class module for the `SimpleGeonamesAnnotator` class.
Please see `help(SimpleGeonamesAnnotator)` for more information.
"""

from typing import Sequence, Dict, Any

from .annotator import Annotator


class SimpleGeonamesAnnotator(Annotator):
    """
    An example annotator with very limited capabilities for annotating city names.

    Supported city names are:
    new york
    paris
    tokyo
    amsterdam
    berlin
    moscow
    mexico city
    athens
    london
    new delhi
    """
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    geonamesToId = {
        "new york": 1,
        "paris": 2,
        "tokyo": 3,
        "amsterdam": 4,
        "berlin": 5,
        "moscow": 6,
        "mexico city": 7,
        "athens": 8,
        "london": 9,
        "new delhi": 10
    }

    def annotate(self, tokens: Sequence[str]) -> Dict[str, Any]:
        """
        Return a dictionary representing the semantics of the annotated tokens.

        The returned semantics are a dictionary of the shape:
        {'name': str, 'id': int}
        """
        geoname = " ".join(tokens).lower()
        geoId = self.geonamesToId.get(geoname, None)

        annotation = None
        if geoId is not None:
            annotation = {"name": geoname, "id": geoId}

        return annotation
