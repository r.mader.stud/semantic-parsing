"""
Class module for the `Annotator` class.
Please see `help(Annotator)` for more information.
"""

from abc import ABC, abstractmethod
from typing import Sequence, Dict, Any


class Annotator(ABC):
    """An abstract base class for annotators."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    @abstractmethod
    def annotate(self, tokens: Sequence[str]) -> Dict[str, Any]:
        """Return a dictionary representing the semantics of the annotated tokens."""
        raise Exception("Not implemented")
