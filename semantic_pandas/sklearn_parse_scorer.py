"""
Class module for the `SklearnParseScorer` class.
Please see `help(SklearnParseScorer)` for more information.
"""


from typing import Callable, Sequence

from .parse import Parse


class SklearnParseScorer:
    """Parse scorer using sklearn models for parsing."""

    def __init__(
            self,
            model,
            toFeatureVector: Callable[[Parse], Sequence[float]],
            transformModels=None):
        """Please see `help(SklearnParseScorer)` for more information."""
        self.model = model
        self.toFeatureVector = toFeatureVector
        self.transformModels = [] if transformModels is None else transformModels

    def __call__(self, parse: Parse) -> float:
        featureVector = self.toFeatureVector(parse)
        featureVector = self.transformFeatureVectors([featureVector])[0]
        score = self.model.predict([featureVector])[0]
        return score

    def transformFeatureVectors(
                self,
                featureVectors: Sequence[Sequence[float]]) -> Sequence[Sequence[float]]:
        """Transform a feature vector by applying the transform models in order."""
        for transformModel in self.transformModels:
            featureVectors = transformModel.transform(featureVectors)

        return featureVectors

    def train(
            self,
            featureVectors: Sequence[Sequence[float]],
            resultVector: Sequence[float]) -> float:
        """Train scoring and transform models using the given training data."""

        # train transform models and transform feature vectors for subsequent training
        for transformModel in self.transformModels:
            featureVectors = transformModel.fit_transform(featureVectors)

        # train scoring model
        self.model.fit(featureVectors, resultVector)

        # return final score of training data set
        return self.model.score(featureVectors, resultVector)
