"""Module for utility functions for the language server."""

import re

from .constants import SEMANTIC_PARSE_PREFIX, TAB_STOP_PLACEHOLDER_RE


def extractIntentFromLine(line: str) -> str:
    """Extract the actual NL utterance from the input document line."""
    intent = line.strip(" \n\t")
    intent = intent[len(SEMANTIC_PARSE_PREFIX):]  # remove prefix

    return intent


def insertTabStops(snippet: str):
    """Insert VS Code tab stops for placeholders in the given snippet."""

    for i, match in enumerate(re.finditer(TAB_STOP_PLACEHOLDER_RE, snippet)):
        matchedString = match.group(0)
        placeholderLabel = match.group(1)

        snippet = snippet.replace(matchedString, f"${{{i}:{placeholderLabel}}}", 1)

    return snippet
