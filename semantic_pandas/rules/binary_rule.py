"""
Class module for the `BinaryRule` class.
Please see `help(BinaryRule)` for more information.
"""

from typing import Union, Sequence

from ..semantics.semantic import Semantic
from ..utility import tokenize
from .rule import Rule


class BinaryRule(Rule):
    """Rule with exactly two right hand side categories."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self, lhs: str, rhs: Union[str, Sequence[str]], semantic: Semantic = None):
        """Please see `help(BinaryRule)` for more information."""
        if isinstance(rhs, str):
            rhs = tuple(tokenize(rhs))

        if len(rhs) != 2:
            raise ValueError(f"Binary rule rhs must have exactly two tokens. Got {len(rhs)}")

        super().__init__(lhs, rhs, semantic)
