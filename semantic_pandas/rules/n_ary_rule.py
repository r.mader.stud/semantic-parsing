"""
Class module for the `NAryRule` class.
Please see `help(NAryRule)` for more information.
"""

from __future__ import annotations
from typing import Iterable, Union
import uuid

from ..semantics.semantic import Semantic
from ..semantics.custom_semantic import CustomSemantic

from . import functions as func
from .rule import Rule
from .binary_rule import BinaryRule


class NAryRule(Rule):
    """Rule helper class representing a rule with more than two categories on its rhs."""
    def __init__(self, lhs: str, rhs: Iterable[str], semantic: Semantic = None):
        """Please see `help(NAryRule)` for more information."""
        if len(rhs) <= 2:
            raise ValueError(
                "rhs must be more than three tokens. "
                "use unary or binary rule types instead"
            )
        super().__init__(lhs, tuple(rhs), semantic)

    def binarize(self) -> (BinaryRule, Union[BinaryRule, NAryRule]):
        """Binarize this rule by creating a corresponding binary rule and (n-1)-ary rule."""
        rhsSemantic = CustomSemantic(lambda sems: sems)

        binarizedCategory = f"{self.lhs}_{self.rhs[0]}"

        # crude way of generating a unique ID in order to prevent ambigous binarized categories
        uid = str(uuid.uuid4())[0:4]
        binarizedCategory = binarizedCategory + f"_{uid}"
        lhsRule = func.makeRule(
            self.lhs,
            (self.rhs[0], binarizedCategory),
            CustomSemantic(lambda sems: self.semantic.compute([sems[0]] + list(*sems[1:])))
        )
        rhsRule = func.makeRule(binarizedCategory, self.rhs[1:], rhsSemantic)

        return (lhsRule, rhsRule)
