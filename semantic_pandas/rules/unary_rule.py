"""
Class module for the `UnaryRule` class.
Please see `help(UnaryRule)` for more information.
"""

from typing import Union, Tuple

from ..semantics.semantic import Semantic
from .rule import Rule
from ..utility import isCategory


class UnaryRule(Rule):
    """Rule with exactly one right hand category."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self, lhs: str, rhs: Union[str, Tuple[str]], semantic: Semantic = None):
        """Please see `help(UnaryRule)` for more information."""

        if isinstance(rhs, tuple):
            if len(rhs) != 1:
                raise Exception(
                    f"RHS tuple must be of length 1 for unary rules. "
                    f"Received length tuple with length {len(rhs)}"
                )

            rhs = rhs[0]

        if not isCategory(rhs):
            raise ValueError(f"rhs must be a category for unary rule, not a lexical value ({rhs})")

        super().__init__(lhs, (rhs,), semantic)
