"""Module containing handcrafted non domain specific queries dataset for parser testing/training."""

import random

random.seed(740273947)

otherQueries = [
    "an apple is a fruit",
    "select the right answer",
    "create new column",
    "how big is new york",
    "convert to float",
    "fastest way to sort a dataframe",
    "get column by number in pandas",
    "select columns from groupby object in pandas",
    "select columns from groupby object",
    "concatenate a series onto a dataframe",
    "join dataframes on index",
    "create new object",
    "gratitude is not the same as indebtedness",
    "convert is not the same as transform"
]

random.shuffle(otherQueries)

trainingOtherQueries = \
    otherQueries[int(len(otherQueries) * 0.1):]

testingOtherQueries = \
    otherQueries[:int(len(otherQueries) * 0.1)]
