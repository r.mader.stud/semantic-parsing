"""Module containing handcrafted pandas queries dataset for parser testing/training."""

import random

from ..pandas_query import PandasQuery


random.seed(3042330287)  # use seed for reproducable results

manualPandasQueries = [
    # create dataframe
    PandasQuery("new dataframe", "DataFrame()"),
    PandasQuery("new dataframe and assign to df", "df = DataFrame()"),
    PandasQuery("create new dataframe", "DataFrame()"),
    PandasQuery("create dataframe from dict", "DataFrame(dict)"),
    PandasQuery("create dataframe df from dict", "df = DataFrame(dict)"),
    PandasQuery(
        "create dataframe df from dict with index ind",
        "df = DataFrame(dict, index=ind)"
    ),
    PandasQuery(
        "create dataframe df from dict with index",
        "df = DataFrame(dict, index={index})"
    ),

    # select column(s)
    #   single column
    PandasQuery("select column from dataframe", "{dataframe}[{column}]"),
    PandasQuery("select column from dataframe as result", "result = {dataframe}[{column}]"),
    PandasQuery("retrieve column from dataframe", "{dataframe}[{column}]"),
    PandasQuery("get column", "{dataframe}[{column}]"),
    PandasQuery("select column col from dataframe", "{dataframe}[col]"),
    PandasQuery("select column 'colName' from dataframe", "{dataframe}['colName']"),
    PandasQuery(
        "select column 'colName' from dataframe and assign to result",
        "result = {dataframe}['colName']"
    ),
    PandasQuery("get column 'colName' from dataframe", "{dataframe}['colName']"),
    PandasQuery("select column from dataframe df", "df[{column}]"),
    PandasQuery("select column from df", "df[{column}]"),
    PandasQuery("get column 'id' from dataframe df", "df['id']"),
    PandasQuery("get column 'id' from dataframe df as result", "result = df['id']"),
    PandasQuery("get column col from dataframe df", "df[col]"),
    PandasQuery("select columns col from dataframe df", "df[col]"),
    PandasQuery("retrieve columns 'colName' from dataframe df", "df['colName']"),

    #   multiple columns
    PandasQuery("select columns 'col1', 'col2' from dataframe df", "df[['col1', 'col2']]"),
    PandasQuery(
        "select columns 'col1', 'col2' from dataframe df as result",
        "result = df[['col1', 'col2']]"
    ),
    PandasQuery("get columns 'col1', 'col2' from dataframe df", "df[['col1', 'col2']]"),
    PandasQuery("get columns 'col1', 'col2' from df", "df[['col1', 'col2']]"),
    PandasQuery("retrieve columns 'col1', 'col2' from dataframe df", "df[['col1', 'col2']]"),
    PandasQuery(
        "retrieve columns 'col1', 'col2' from dataframe df as result",
        "result = df[['col1', 'col2']]"
    ),
    PandasQuery(
        "retrieve columns 'col1', 'col2' from dataframe df and assign to result",
        "result = df[['col1', 'col2']]"
    ),

    # select rows
    #   by logical condition
    PandasQuery("select rows", "{dataframe}[{condition}]"),
    PandasQuery("select rows from dataframe", "{dataframe}[{condition}]"),
    PandasQuery("retrieve rows from dataframe as result", "result = {dataframe}[{condition}]"),
    PandasQuery("select rows from dataframe df", "df[{condition}]"),
    PandasQuery("get rows from dataframe df", "df[{condition}]"),
    PandasQuery("get rows from dataframe df as result", "result = df[{condition}]"),
    PandasQuery("select rows where var1 greater than var2", "{dataframe}[var1 > var2]"),
    PandasQuery(
        "select rows where var1 greater than var2 and assign to result",
        "result = {dataframe}[var1 > var2]"
    ),
    PandasQuery("select rows where var1 is greater than var2", "{dataframe}[var1 > var2]"),
    PandasQuery("retrieve rows where var1 is greater than var2", "{dataframe}[var1 > var2]"),
    PandasQuery("select rows from df where var1 greater than var2", "df[var1 > var2]"),
    PandasQuery(
        "select rows from df where var1 greater than var2 and assign to result",
        "result = df[var1 > var2]"
    ),
    PandasQuery("get rows from df where var1 greater than var2", "df[var1 > var2]"),

    #   by position
    PandasQuery("select rows 10 to 20", "{dataframe}[10:20]"),
    PandasQuery("select rows from dataframe df 5 to 15", "df[5:15]"),
    PandasQuery("select rows from dataframe df 5 to 15 and assign to range", "range = df[5:15]"),

    #   first n
    PandasQuery("select first 4 rows", "{dataframe}.head(4)"),
    PandasQuery("select first 4 rows and assign to head", "head = {dataframe}.head(4)"),
    PandasQuery("select first 7 rows of df", "df.head(7)"),

    #   last n
    PandasQuery("select last 10 rows", "{dataframe}.tail(10)"),
    PandasQuery("select last 23 rows of df", "df.tail(23)"),
    PandasQuery("select last 23 rows of df as tail", "tail = df.tail(23)"),

    # convert dataframe
    PandasQuery("convert dataframe to csv", "{dataframe}.to_csv()"),
    PandasQuery("convert df to csv", "df.to_csv()"),
    PandasQuery("convert dataframe to dict", "{dataframe}.to_dict()"),
    PandasQuery("convert dataframe df to dict as mydict", "mydict = df.to_dict()"),
    PandasQuery("convert dataframe to json", "{dataframe}.to_json()"),
    PandasQuery("convert dataframe to numpy", "{dataframe}.to_numpy()"),
    PandasQuery("convert df to numpy", "df.to_numpy()"),
    PandasQuery("convert df to numpy and assign to result", "result = df.to_numpy()"),
    PandasQuery("convert dataframe df to csv", "df.to_csv()"),
    PandasQuery("convert dataframe df to csv as mycsv", "mycsv = df.to_csv()"),

    # append dataframes
    #   using rows (concat([df1, df2]))
    PandasQuery("append rows of df1 to df2", "concat([df1, df2])"),
    PandasQuery("concat rows of df1 to df2", "concat([df1, df2])"),
    PandasQuery("concat rows of df1 to df2 and assign to result", "result = concat([df1, df2])"),
    PandasQuery("concat df1 and df2", "concat([df1, df2])"),
    PandasQuery("append df1 to df2", "concat([df1, df2])"),
    PandasQuery("append df1 to df2 as result", "result = concat([df1, df2])"),

    #   using columns (concat([df1, df2], axis=1))
    PandasQuery("append columns of df1 to df2", "concat([df1, df2], axis=1)"),
    PandasQuery("append columns df1 to df2", "concat([df1, df2], axis=1)"),
    PandasQuery("append columns df1 to df2 as result", "result = concat([df1, df2], axis=1)"),
    PandasQuery("concat columns of df1 to df2", "concat([df1, df2], axis=1)"),
    PandasQuery("concat columns df1 and df2", "concat([df1, df2], axis=1)"),
    PandasQuery(
        "concat columns df1 and df2 and assign to result",
        "result = concat([df1, df2], axis=1)"
    ),

    # sort dataframe (df.sort_values('column'))
    PandasQuery("sort df", "df.sort_values()"),
    PandasQuery("sort df by col", "df.sort_values(col)"),
    PandasQuery("sort df by col1, col2", "df.sort_values([col1, col2])"),
    PandasQuery("sort df by col1, col2, col3", "df.sort_values([col1, col2, col3])"),
    PandasQuery("sort by col", "{dataframe}.sort_values(col)"),
    PandasQuery("sort by 'col'", "{dataframe}.sort_values('col')"),
    PandasQuery("sort df by 'col'", "df.sort_values('col')"),

    # reduce column
    #   into max
    PandasQuery("select maximum", "{dataframe}.max()"),
    PandasQuery("select maxima of df", "df.max()"),
    PandasQuery("select maximum from col in df", "df[col].max()"),
    PandasQuery("retrieve maximum from col in df", "df[col].max()"),
    PandasQuery("select maximum from col in df as max", "max = df[col].max()"),
    PandasQuery("select maximum from col in df as result", "result = df[col].max()"),
    PandasQuery("select maximum in df from col", "df[col].max()"),
    PandasQuery("get maximum from col in df", "df[col].max()"),

    #   into min
    PandasQuery("select minimum", "{dataframe}.min()"),
    PandasQuery("select minima of df", "df.min()"),
    PandasQuery("get minima of df", "df.min()"),
    PandasQuery("retrieve minima of df", "df.min()"),
    PandasQuery("select minimum from col in df", "df[col].min()"),
    PandasQuery("get minimum from col in df as min", "min = df[col].min()"),
    PandasQuery("select minimum from col in df and assign to min", "min = df[col].min()"),
    PandasQuery("retrieve minimum from col in df as result", "result = df[col].min()"),
    PandasQuery("get minimum from col in df", "df[col].min()"),
    PandasQuery("select minimum in df from col", "df[col].min()"),

    #   into mean
    PandasQuery("get average", "{dataframe}.mean()"),
    PandasQuery("get average as result", "result = {dataframe}.mean()"),
    PandasQuery("retrieve average as result", "result = {dataframe}.mean()"),
    PandasQuery("retrieve average and assign to result", "result = {dataframe}.mean()"),
    PandasQuery("get average from df", "df.mean()"),
    PandasQuery("select mean from col in df", "df[col].mean()"),
    PandasQuery("select average from col in df", "df[col].mean()"),
    PandasQuery("select average from col in df and assign to mean", "mean = df[col].mean()"),
    PandasQuery("get average from col in df", "df[col].mean()"),
    PandasQuery("get mean from col in df", "df[col].mean()"),
]

random.shuffle(manualPandasQueries)

trainingManualPandasQueries = \
    manualPandasQueries[int(len(manualPandasQueries) * 0.1):]

testingManualPandasQueries = \
    manualPandasQueries[:int(len(manualPandasQueries) * 0.1)]
