"""Module containing datasets for training/testing semantic parsers."""

from .manual_pandas_queries import (manualPandasQueries,
                                    trainingManualPandasQueries,
                                    testingManualPandasQueries)

from .other_queries import (otherQueries,
                            trainingOtherQueries,
                            testingOtherQueries)

__all__ = [
    "manualPandasQueries",
    "trainingManualPandasQueries",
    "testingManualPandasQueries",
    "otherQueries",
    "trainingOtherQueries",
    "testingOtherQueries"
]
