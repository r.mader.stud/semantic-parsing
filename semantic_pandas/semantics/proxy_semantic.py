"""
Class module for the `ProxySemantic` class.
Please see `help(ProxySemantic)` for more information.
"""

from typing import Sequence, Dict, Any

from .semantic import Semantic


class ProxySemantic(Semantic):
    """Semantic for forwarding one input semantics to the next semantic."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self, position: int = 0):
        self.position: int = position

    def compute(self, semantics: Sequence[Dict[str, Any]]) -> Dict[str, Any]:
        """Return the input semantics at the given position."""
        return semantics[self.position]
