"""
Class module for the `SimpleSemantic` class.
Please see `help(SimpleSemantic)` for more information.
"""

from typing import Any, Dict, Sequence

from .semantic import Semantic


class SimpleSemantic(Semantic):
    """Semantic for creating a new semantic independant of the input semantics."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(self, semantic: Dict[str, Any]):
        self.semantic = semantic

    def compute(self, semantics: Sequence[Dict[str, Any]]) -> Dict[str, Any]:
        """
        Return the semantics value used to construct this semantic.

        Note that the input semantics are ignored.
        """
        return self.semantic
