"""
Class module for the `NestedCombineSemantic` class.
Please see `help(NestedCombineSemantic)` for more information.
"""

from typing import Any, Dict, Iterable, Sequence, Mapping

from .semantic import Semantic
from .nested_semantic import NestedSemantic
from .combine_semantic import CombineSemantic


class NestedCombineSemantic(Semantic):
    """Apply a nesting and combining operation on the input semantics."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(
            self,
            labelMapping: Mapping[str, int],
            combinePositions: Iterable[int],
            additionals: Dict[str, Any] = None):
        """Please see `help(NestedCombineSemantic)` for more information."""
        self.nestedSemantic = NestedSemantic(labelMapping)

        # NOTE: first position in the combine semantic is the result of the nested semantic,
        # therefore we have to increase all positions by one
        self.combineSemantic = CombineSemantic(
            [0] + [i + 1 for i in combinePositions],
            additionals
        )

    def compute(self, semantics: Sequence[Dict[str, Any]]) -> Dict[str, Any]:
        """Compute the semantics by computing a `NestedSemantic` and `CombineSemantic`."""
        nested = self.nestedSemantic.compute(semantics)
        return self.combineSemantic.compute([nested] + semantics)
